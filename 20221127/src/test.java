import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-27
 * Time: 14:26
 */
public class test {
    public static String toFindString (String str,int pos) {
        String ret = "";
        for (int i = pos+1; i < str.length()-1; i++) {
            char ch = str.charAt(i);
            int count = 0;
            while ((str.charAt(pos) == str.charAt(i))&&(i <= str.length())) {
                ret += ch;
                pos++;
                i++;
                count++;
            }
        }
        return ret;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        while (scanner.hasNextLine()) {
            for (int i = 0; i < s.length()-1; i++) {
                String ret = toFindString(s,i);
                System.out.println(ret);
            }
        }
    }
}
