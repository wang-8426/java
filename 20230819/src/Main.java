import java.util.*;

public class Main {


    public static String solve(int M, int N) {
        // write code here
        if(M == 0){
            return "0";
        }
        //要考虑 M 为负数的情况
        boolean flag = true;
        if(M < 0){
            M = -M;
            flag = false;
        }

        StringBuilder res = new StringBuilder();
        String hex = "0123456789ABCDEF";

        while(M !=0){
            res.append(hex.charAt(M%N));
            M = M / N;
        }
        // 字符串反转
        return flag ? res.reverse().toString():"-"+res.reverse().toString();
    }

    public static void main(String[] args) {

        int M;
        int N;
        Scanner sc =new Scanner(System.in);
        M = sc.nextInt();
        N = sc.nextInt();

        String A = solve(M, N);
        System.out.println(A);

    }
}