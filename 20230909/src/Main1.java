import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-09-09
 * Time: 下午 11:13
 */
class Solution {
    public int longestConsecutive(int[] nums) {


        Set< Integer > numSet = new HashSet<>();
        int best = 0;  //最长序列长度
        int len = 0;  //序列长度

        //将数组中的值放入哈希表中
        for (int num : nums) {
            numSet.add(num);
        }
        //
        for (int number : numSet) {
            //判断一个数是不是其所在序列的起点
            if (!numSet.contains(number - 1)) {
                len = 1;
                //判断一个数后面还有没有连续的数
                while (numSet.contains(++number)) {
                    len++;
                }
            }
            best = Math.max(len, best);
        }
        return best;

    }
}