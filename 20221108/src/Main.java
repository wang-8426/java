/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-08
 * Time: 21:46
 */
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            Data data = new Data(x, y);
            System.out.println(data.getX() + data.getY());
        }
    }

}

class Data {

    private int x;
    private int y;

     Data(int x, int y) {
        this.x = x;
        this.y = y;
    }

     public int getX() {
        return x;
    }

     public int getY() {
        return y;
    }

}