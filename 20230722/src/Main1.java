import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-22
 * Time: 下午 12:40
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int ret1 = num/2;
        int ret2 = num/2;

        int a = num/2;
        int b = num/2;
        while(a>=2 && b<num){
            if(isPrime(a) && isPrime(b)){
                System.out.println(a);
                System.out.println(b);
                break;
            }else{
                a--;
                b++;
            }
        }

    }
    public static boolean isPrime(int num) {
        int j ;
        for ( j = 2; j < num; j++) {
            if(num % j == 0) {
                return false;
            }
        }
        return true;
    }
}
