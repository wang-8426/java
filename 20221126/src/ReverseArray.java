/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-26
 * Time: 21:29
 */
public class ReverseArray {
    public static void Reverse(int[] arr) {
        int start = 0;
        int tmp = 0;
        int end = arr.length-1;
        while(start < end) {
            tmp = arr[start];
            arr[start] = arr[end];
            arr[end] = tmp;
            start++;
            end--;
        }
        //return arr;
    }
    public static void main1(String[] args) {
        int[] array = new int[]{1,2,3,4,5,6};
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();

        Reverse(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+" ");
        }
    }
}
