/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-26
 * Time: 22:23
 */
public class toLowerCase {
    public static String toLowerCase(String s) {
        String ret ="";
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch >= 'A'&& ch <= 'Z') {
                ch += 32;
            }
            ret+=ch;
        }
        return ret;
    }
    public static void main(String[] args) {
        String s = toLowerCase("DAWD");
        System.out.println(s);
    }
}
