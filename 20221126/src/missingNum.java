/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-26
 * Time: 21:57
 */
public class missingNum {
    public static int missingNumber(int[] nums) {
        int sum = 0;
        sum = nums.length*(nums.length+1)/2;
        for (int i = 0; i < nums.length; i++) {
            sum-=nums[i];
        }
        return sum;

    }
    public static void main(String[] args) {
        int[] array = new int[]{5,6,4,2,1,0};
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+" ");

        }
        System.out.println();
        System.out.println(missingNumber(array));
    }
}
