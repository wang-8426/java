import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(count(n));
    }
//第一 二个月兔子的数量都是一，再往后每个月的兔子数量是前两个月之和
    private static int count(int n) {
        if(n <= 2) {
            return 1;
        } else {
            return count(n-1)+count(n-2);
        }
    }
}