import java.util.Scanner;
public class Main1{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String regex = in.next().toLowerCase();
            String target = in.next().toLowerCase();

            boolean dp[][]=new boolean[target.length() + 1][regex.length() + 1];
            dp[0][0]=true;
            for(int i = 1; i <= target.length(); i++){
                if (regex.charAt(i - 1) == '*') {
                    dp[0][i] = true;
                } else {
                    break;
                }
            }

            for(int i = 1; i <= target.length(); i++){
                for(int j = 1 ; j <= regex.length(); j++){
                    if (target.charAt(i - 1) == regex.charAt(j - 1)) {
                        dp[i][j]=dp[i-1][j-1];
                    } else if (Character.isDigit(target.charAt(i - 1)) || Character.isLetter(target.charAt(i - 1))) {
                        if (regex.charAt(j - 1) == '*') {
                            dp[i][j] = dp[i - 1][j - 1] || dp[i - 1][j] || dp[i][j - 1];
                        } else if (regex.charAt(j - 1) == '?') {
                            dp[i][j] = dp[i - 1][j - 1];
                        }
                    }
                }
            }
            System.out.println(dp[target.length()][regex.length()]);
        }
    }
}
