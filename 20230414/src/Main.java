import java.util.*;

public class Main
{
    public int getValue(int[] gifts, int n) {
        int value = gifts[0];
        int count = 1;
        for(int i = 1; i < n; i++)
        {
            if(gifts[i] != value)
                count--;
            else
                count++;
            if(count == 0)
                value = gifts[i];
        }

        count = 0;
        for(int i = 0; i < n; ++i) {
            if(gifts[i] == value)
                count++;
        }
        if(count < n/2)
            return 0;
        else
            return value;

        }
    }
}