import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-05
 * Time: 20:09
 */
public class test {
    //冒泡排序
    public static void main(String[] args) {
        int[] array1 = {5,7,8,4,5,2,6,8,3,1};
    }

    //二分查找
    public static int binarySerach(int[] array, int n) {
        int left = 0;
        int right = array.length - 1;
        int mid = 0;
        while(left <= right) {
            mid = (left + right)/2;
            if(n > array[mid]) {
                left = mid - 1;
            } else if (n < array[mid]) {
                right = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }
    public static void main3(String[] args) {
        int[] array1 = {1,2,4,5,6,7,8,9,10};
        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();
        int ret = binarySerach(array1,x);
        if(ret == -1) {
            System.out.println("没找到该数字！");
        } else {
            System.out.println("找到了，下标是"+ret);
        }
    }

    //奇数位于偶数之前
    public static void main2(String[] args) {
        int[] array1 = new int []{1,2,3,4,5,6,7};
        //创建一个新的数组
        int[] array2 = new int [array1.length];
        int a = 0;
        int b = array1.length - 1;
        //遍历数组
        for (int i = 0; i < array1.length; i++) {
            //找到偶数放array2后边
          if(array1[i] % 2 == 0) {
              array2[b] = array1[i];
              b--;
              //找到奇数放array2前边
          } else {
              array2[a] = array1[i];
              a++;
          }
        }
        //输出array1，array2
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }
    //改变数组中的值
    public static void transform(int[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i]*2;
        }
    }
    public static void main1(String[] args) {
        int[] array1 = new int[]{1,2,3,4};
        transform(array1);
        for (int x:array1) {
            System.out.print(x+" ");
        }
    }
}
