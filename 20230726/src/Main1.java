/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-26
 * Time: 上午 09:58
 */
public class Main1 {
    public int findMinimum(int n, int[] left, int[] right) {
        // write code here
        int sum = 0;//保存无法匹配的手套数!
        int sumleft = 0; //左手手套数!
        int sumright = 0;//右手手套数!
        int leftMin = Integer.MAX_VALUE;//保存左右手手套最小值!
        int rightMin = Integer.MAX_VALUE;
        for(int i = 0;i<left.length;i++){
            if(left[i]*right[i]==0){
                //不匹配手套,
                sum += (left[i]+right[i]);
            }else{
                sumleft += left[i];
                sumright += right[i];
                if(left[i]<leftMin){
                    //更新最小值
                    leftMin = left[i];
                }
                if(right[i]<rightMin){
                    //更新最小值
                    rightMin = right[i];
                }
            }
        }
        return sum + Math.min(sumleft-leftMin+1,sumright-rightMin+1)+1;

    }
}
