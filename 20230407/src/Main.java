import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-07
 * Time: 18:22
 */
public class Main {
    public static boolean judge (int x) {
        int count = 0;
        for (int i = 1; i < x; i++) {
            if(x % i == 0){
                count += i;
            }


        }
        if(count == x) {
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int count = 0;
        for (int i = 1; i < n; i++) {
            if (judge(i)) {
                count++;
            }

        }
        System.out.println(count);

    }
}
