import java.util.Locale;
import java.util.Scanner;

import static jdk.nashorn.internal.objects.NativeString.toLowerCase;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-07
 * Time: 22:14
 */
public class Main3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = new String(sc.nextLine());
        sc.hasNextLine();
        String a = sc.nextLine();
        String lowerstr = toLowerCase(str);
        char ch = a.charAt(0);
        int count = 0;
        for (int i = 0; i < lowerstr.length(); i++) {
            if (lowerstr.charAt(i) == Character.toLowerCase(ch)) {
                count++;
            }
        }
        System.out.println(count);
    }
}
