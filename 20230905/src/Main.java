/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-09-05
 * Time: 下午 10:05
 */
class Solution {
    public int massage(int[] nums) {
        int n = nums.length;
        if(n == 0) {
            return 0;
        }

        int[] f = new int[n];
        int[] g = new int[n];
        f[0] = nums[0];
        for(int i = 1;i < n;i++) {
            f[i] = g[i-1] + nums[i];
            g[i] = Math.max(g[i-1],f[i-1]);
        }
        return Math.max(g[n-1],f[n-1]);

    }
}