/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-20
 * Time: 23:39
 */
public abstract class Vehicle {

    public Vehicle(int speed, int fare, String range,int capacity) {
        this.speed = speed;
        this.fare = fare;
        this.range = range;
        this.capacity = capacity;
    }

    protected int speed;
    protected int fare;
    protected String range; //市内 省内 国外
    protected int capacity;

    abstract  void move();

}
class subway extends Vehicle{
    protected int capacity = 200;

    public subway(int speed,int fare,String range,int capacity) {
        super(speed,fare,range,capacity);
    }

    void move() {
        System.out.println("载"+capacity+"人在地下跑");
    }
}
class plane extends Vehicle {


    public plane (int speed,int fare,String range,int capacity) {
        super(speed,fare,range,capacity);
    }
    void move() {
        System.out.println("在天上飞");
    }
}

class train extends Vehicle {

    public train (int speed,int fare,String range,int capacity) {
        super(speed,fare,range,capacity);
    }

    void move() {
        System.out.println("在铁路上跑");
    }


    public static void main(String[] args) {
        Vehicle vehicle = new subway(150,5,"市内",200);
        vehicle.move();
    }
}


