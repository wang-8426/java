import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-04
 * Time: 21:32
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int year = sc.nextInt();
        int month = sc.nextInt();
        int day = sc.nextInt();
        int days = 0;
        int[] months = new int[]{31, 28 ,31,30,31,30,31,31,30,31,30,31};
        if((year % 400 == 0) || (year % 4 == 0 && year%100 != 0)) {
            months[1] = 29 ;
        }
        if(month == 0) {
            days = day;
            System.out.println(days);
            return;
        }
        for (int i = 0; i < month-1; i++) {
                days += months[i];
        }
        days += day;
        System.out.println(days);
    }
}
