/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-20
 * Time: 上午 09:54
 */
public class Main {

    public static boolean checkWon(int[][] board) {
        // write code here
        for (int i = 0; i < 3; i++) {
            if(board[i][0]+board[i][1]+board[i][2] == 3){
                return true;
            }
        }
        for (int j = 0; j < 3; j++) {
            if (board[0][j] + board[1][j] + board[2][j] == 3) {
                return true;
            }
        }
        int count = 0;
        for (int i = 0, j = 0; i < 3; i++,j++) {
            count += board[i][j];
            if(count == 3) {
                return true;
            }
        }
        return false;

    }
}
