import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-20
 * Time: 上午 10:30
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.next();
        int score = security(str);
        if(score >= 90) System.out.println("VERY_SECURE");
        else if(score >= 80) System.out.println("SECURE");
        else if(score >= 70) System.out.println("VERY_STRONG");
        else if(score >= 60) System.out.println("STRONG");
        else if(score >= 50) System.out.println("AVERAGE");
        else if(score >= 25) System.out.println("WEAK");
        else System.out.println("VERY_WEAK");
    }

    public static int security(String str) {
        int count = 0;
        //1.长度分数
        if (str.length() <= 4) count += 5;
        else if (str.length() <= 7) count += 10;
        else count +=25;

        //2.字母 数字 符号分数
        int lowCount = 0;
        int upCount = 0;
        int num = 0;
        int sign = 0;

        for (int i = 0; i < str.length(); i++) {
            if(Character.isLowerCase(str.charAt(i))) lowCount++;
            else if(Character.isUpperCase(str.charAt(i))) upCount++;
            else if(Character.isDigit(str.charAt(i))) num++;
            else  sign++;
        }
        //字母分数
        if ((lowCount == 0 && upCount > 0)||
            (lowCount > 0 && upCount ==0))
            count += 10;
        else if (lowCount > 0 && upCount > 0) count +=20;
        else count += 0;


        //数字分数
        if (num == 1) count += 10;
        else if (num > 1) count += 20;
        else count += 0;


        //符号分数
        if (sign == 1) count += 10;
        else if (sign > 1) count += 25;
        else count += 0;

        //奖励分数
        if (num > 0 && upCount > 0 && lowCount > 0 && sign > 0) count += 5;
        else if(num > 0 && sign > 0 &&(upCount >0 || lowCount >0)) count += 3;
        else if(num > 0 &&(upCount >0 || lowCount >0)) count += 2;
        return count;

    }
}
