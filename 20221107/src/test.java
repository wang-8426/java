import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-07
 * Time: 21:50
 */
public class test {
    //数组逆序
    public static int[] reverse(int[] array) {
        int left = 0;
        int right = array.length-1;
        int tmp = 0;
        while(left <= right) {
            tmp = array[left];
            array[left] = array[right];
            array[right] = tmp;
            left++;
            right--;
        }
        return array;
    }
    public static void main(String[] args) {

        int[] array1 = {1,2,4,5,6,7};
        int[] ret = reverse(array1);
        System.out.println(Arrays.toString(ret));
    }


}
