import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-09
 * Time: 下午 02:11
 */
public class Main3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()){

            double x = sc.nextDouble();
            double y = sc.nextDouble();
            double z = sc.nextDouble();
            if(x+y >z && x+z>y && y+z>x ) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
