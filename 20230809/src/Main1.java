import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-09
 * Time: 下午 01:02
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            double n = sc.nextDouble();
            double r = sc.nextDouble();
            if(6.28*r > n) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
