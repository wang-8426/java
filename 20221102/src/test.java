import javax.security.sasl.SaslClient;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-02
 * Time: 9:05
 */
public class test {
    public static void main1(String[] args) {
        //登录密码
        String password = "20210701";
        Scanner scan = new Scanner(System.in);
        int flag = 3;
        while(flag > 0) {
            System.out.println("输入密码：");
            String land = scan.nextLine();
            //a.equals(s)用来比较两个字符串是否相等
            if(land.equals(password)){

                System.out.println("登陆成功！");
                break;
            } else {
                System.out.println("密码错误,还有"+(flag-1)+"次机会");
                flag--;
            }
        }
    }

    public static int  Max (int a,int b){
        return a > b ? a : b;
    }
    public static double  Max (double a,double b){
        return a > b ? a : b;
    }

    public static double Max(double a, double b, double c) {
        return Max(Max(a, b), c);
    }
    public static int Max(int a, int b, int c) {
        return Max(Max(a, b), c);
    }

    public static void main(String[] args) {
        //求3个或2个数的最大值
        //System.out.println(Max(5, 8, 3));
        System.out.println(Max(2.5 , 4.1));

    }
    public static void main2(String[] args) {
        //用方法求两个数最大值
        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();
        int y = scan.nextInt();
        System.out.println(Max(x, y));

    }

    public static int Fin(int n){
        if (n == 1) {
            return 1;
        } else if(n == 2) {
            return 1;
        }
        return Fin(n - 1) + Fin(n - 2);
    }

    public static void main4(String[] args) {
        //求n项斐波那契数列（递归）
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.println(Fin(n));
    }

    //求和重载
    public static int Add(int a, int b) {
        return a + b;
    }
    public  static double Add(int a, double b) {
        return  a + b;
    }
    public static double Add(double a,int b) {

        return a + b;
    }

    public static void main5(String[] args) {
        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();
        int y = scan.nextInt();
        System.out.println(Add(x , y));
    }
}
