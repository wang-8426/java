import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()) {
            String str = sc.nextLine();
            StringBuilder s = new StringBuilder();
            for(int i = 0;i<str.length();i++) {
                char ch = str.charAt(i);
                if (ch >= 'A' && ch <= 'Z'){
                    s.append((char)(ch>'E'?ch-5:ch+21));


                }else{
                    s.append(ch);
                }
            }
            System.out.println(s);
        }
    }
}
