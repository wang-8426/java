import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-15
 * Time: 下午 09:33
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1 = sc.nextLine();
        System.out.println(StrToInt(s1));

    }
    public static int StrToInt(String str) {
        if (str.length()==0){
            return 0;
        }else if (str.length() == 1&&(str.charAt(0)=='-' ||str.charAt(0)=='+' )){
            return 0;
        }else {
            int flag = 0;//1是整数，2是负数
            boolean error = false;
            char[] chars = str.toCharArray();
            int i = 0;
            if (chars[0]=='-'){
                i++;
                flag= 2;
            }else if (chars[0]=='+'){
                i++;
                flag= 1;
            }

            int result = 0;
            for (int j = i; j < chars.length; j++) {

                if (chars[j]>='0'&&chars[j]<='9'){
                    result = result*10+(chars[j]-'0');
                }else {
                    error = true;
                    break;
                }
            }

            if (!error){
                if (flag==2){
                    result = result*(-1);
                }
                return result;
            }else {
                return 0;
            }
        }
    }
}
