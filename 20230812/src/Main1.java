/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-12
 * Time: 下午 10:44
 */
import java.util.Scanner;
public class Main1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int n = in.nextInt();
            int count = 0;
            for(int i = 2;i <= Math.sqrt(n);i++){
                if(n % i == 0){
                    while(n % i == 0){
                        n /= i;
                    }
                    count++;
                }
            }
            if(n != 1){
                count++;
            }
            System.out.println(count);
        }
    }
}
