/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-16
 * Time: 下午 10:11
 */
import java.util.*;
public class Main{
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String str1 = scanner.next();
            String str2 = scanner.next();
            int length2 = str2.length();
            int length1 = str1.length();
            int count = 0;
            for (int i = 0; i < length1; i++) {
                //若第一个字母相同则继续比较下去
                if (str1.charAt(i) == str2.charAt(0)) {
                    boolean flag = true;
                    int j = 0;
                    for (j = 0; j < length2; j++) {
                        if ( i + j < length1&& str2.charAt(j) != str1.charAt(j + i) ) {
                            flag = false;
                            break;
                        }
                    }
                    if ( i + j < length1+1 && flag) {
                        count++;
                        i += length2 - 1;
                    }
                } else {
                    //若第一个字母都不同就跳下一个字母
                    continue;
                }
            }
            System.out.println(count);
        }
    }
}