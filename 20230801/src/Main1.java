import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-01
 * Time: 下午 11:12
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextLine()) { // 注意 while 处理多个 case
            String str1 = in.nextLine();
            String str2 = in.nextLine();
            System.out.println(funHJ75(str1, str2));
        }
    }
    public static int funHJ75(String str1, String str2) {
        int result = 0;
        String max = str1.length() >= str2.length() ? str1 : str2;
        String min = str1.length() >= str2.length() ? str2 : str1;
        //i表示substring的跨度，j表示从哪一位开始切片，暴力遍历破解
        for (int i = 1; i <= min.length(); i++) {
            for (int j = 0; i + j <= min.length(); j++) {
                String temp = min.substring(j, j + i);
                if (max.contains(temp)) {
                    result = temp.length();
                    break;
                }
            }
        }
        return result;
    }
}
