import java.util.Locale;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-01
 * Time: 上午 10:00
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.next().toLowerCase();
        String str1 = "";
        for (int i = str.length()-1; i >= 0; i--) {
            str1 += str.charAt(i);
        }
        System.out.println(str1);
    }
}
