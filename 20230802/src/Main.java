/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-02
 * Time: 上午 11:18
 */
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            String str = in.next();
            int index = 1;
            int start = 1;
            int end = Math.min(4,n);
            for(int i = 0 ; i < str.length() ; i++){
                if(str.charAt(i)=='U'){
                    if(index==1){
                        index = n;
                    }else{
                        index--;
                    }
                }else{
                    index = index % n + 1;
                }
                if(index<start){
                    start = index;
                    end = start + 3;
                }else if(index>end){
                    end = index;
                    start = end - 3;
                }
            }
            for(int i = start ; i <= end ; i++){
                System.out.print(i + " ");
            }
            System.out.println();
            System.out.println(index);
        }
    }
}
