import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-13
 * Time: 18:02
 */
public class Main2{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long  M = scanner.nextLong();
        int N = scanner.nextInt();
        System.out.println(solve(M,N));

    }
    public static String solve(long M, int N) {
        StringBuilder res = new StringBuilder();
        boolean hasopt = false;
        if (M < 0) {
            hasopt = true;
            M = -M;
        }
        while (M != 0) {
            long carry = M % N;
            String appd = "";
            if (carry > 10) {
                appd = String.valueOf((char) (carry - 10 + 'A'));
            } else {
                appd = String.valueOf(carry);
            }
            res.append(appd);
            M = M / N;
        }
        if (hasopt) {
            res.append("-");
        }
        return res.reverse().toString();
    }

}
