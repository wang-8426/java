import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-13
 * Time: 18:02
 */
public class Main3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int A,B,C;
        int x = in.nextInt();
        int y = in.nextInt();
        int z = in.nextInt();
        int k = in.nextInt();

        if ((x+z)%2==0 && (y+k)%2==0 ) {
             A = (x + z) / 2;
             B = (y + k) / 2;
             C = k - B;
             if (A>=0&&B>=0&&C>=0) {
                 System.out.println(A+" "+B+" "+C);

             } else {
                 System.out.println("No");
             }
        } else {
            System.out.println("No");
        }
    }
}
