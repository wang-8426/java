import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-12
 * Time: 21:31
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        int T= sc.nextInt();
        for (int i = 0; i < T; i++) {
            int n=sc.nextInt();
            int k= sc.nextInt();
            int[] arr=new int[2*n];
            //读取
            for (int j = 0; j < 2 * n; j++) {
                arr[j]= sc.nextInt();
            }
            //洗
            for (int j = 0; j < k; j++) {
                wash(arr,2*n);
            }
            //print
            for (int j = 0; j < 2*n-1; j++) {
                System.out.print(arr[j] + " ");
            }
            System.out.println(arr[2*n-1]);
        }
    }

    public static void wash(int[] arr,int n) {
        List<Integer> arrayList=new ArrayList<Integer>();
        for (int i = 0; i < n/2; i++) {
            arrayList.add(arr[i]);
            arrayList.add(arr[i+n/2]);
        }
        for (int i = 0; i < n; i++) {
            arr[i]= arrayList.get(i);
        }
    }
}
