import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-29
 * Time: 下午 09:45
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuffer stringBuffer = new StringBuffer(sc.nextLine());
        String str = stringBuffer.toString();
        System.out.println(reverse1(str));

    }

    private static String reverse1(String str) {
        StringBuilder res = new StringBuilder();
        StringBuilder sb = new StringBuilder();
        for (int i = str.length() - 1; i >= 0 ; i--) {
            char c = str.charAt(i);
            if(Character.isLetter(c)) {
                sb.append(c);
            } else {
                res.append(sb.reverse());
                res.append(' ');
                sb.delete(0,sb.length());
            }
        }
        res.append((sb.reverse()));
        return res.toString();
    }
}
