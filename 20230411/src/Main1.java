import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-11
 * Time: 19:21
 */


public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String n1 = new String(sc.nextLine());
        sc.hasNextInt();
        String n2 = new String(sc.nextLine());
        if(n1.length() > n2.length()) {
            sameString(n2, n1);
        } else {
            sameString(n1, n2);
        }
    }
    public static void sameString(String s1, String s2) {
        String min = s1.length() > s2.length() ? s2 : s1;
        String max = s1.length() > s2.length() ? s1 : s2;

        String res = "";
        int maxLength = -1;
        for (int i = 0; i < min.length(); i++) {
            for (int j = i + 1; j <= min.length(); j++) {
                if (max.contains(min.substring(i, j)) && j - i > maxLength) {
                    maxLength = j - i;
                    res = min.substring(i, j);
                }
            }
        }
        if (maxLength == -1) {
            System.out.println("-1");
        } else {
            System.out.println(res.length());
        }
    }
}