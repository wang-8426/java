package book;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 22:24
 */
public class BookShelf {
    private Book[] books = new Book[10];  //初始定义可以放10本书
    private int usedSize;  //记录已使用的大小

    public BookShelf() {
        books[0] = new Book("狂人日记","鲁迅",50,"小说");
        books[1] = new Book("骆驼祥子","老舍",45,"小说");
        this.usedSize = 2;

    }
    public Book getBook(int pos){

        return this.books[pos];
    }

    public void setBooks(int pos,Book book) { //将books数组中的下标为pos的书更换
        this.books[pos] = book;
    }

    public void setBooks(Book book) {  //在books数组的末尾添加图书


        this.books[usedSize] = book;
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        this.usedSize = usedSize;
    }
}
