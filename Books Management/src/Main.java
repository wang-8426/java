import book.BookShelf;
import user.Administrator;
import user.NormalUser;
import user.User;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 23:27
 */
public class Main {
    public static User login(){
        System.out.println("请输入你的用户名：");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();

        System.out.println("请选择你的身份 -》 0.Administrator   1.NormalUser ");
        int choice = scanner.nextInt();
        if(choice == 0) {
            //使用传参的方式发生向上转型
            return new Administrator(name);

        } else {
            return new NormalUser(name);

        }
    }
    public static void main(String[] args) {
        BookShelf bookShelf = new BookShelf();
        User user = login();
        //这时候要根据不同user调用不同的menu，需要在父类当中创建一个抽象的menu方法（menu在使用之前不知道具体用哪个，就给他抽象）
        while (true) {
            int choice = user.menu();  //menu方法返回了你选择执行的操作
            user.doWork(choice, bookShelf);  //在选择身份后，调用对应下下标的对象的work方法
            System.out.println("  ");

        }

    }
}
