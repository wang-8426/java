package user;

import operate.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 22:39
 */
public class NormalUser extends User{

    public NormalUser(String name) {
        super(name);
        this.ioperations = new Ioperation[] {
                new ExitSystem(),//将退出系统放在数字组的0下标处，与菜单贴合
                new FindBook(),
                new BorrowBook(),
                new ReturnBook(),
        };

    }
    public int menu() {
        System.out.println("----------------------------------");
        System.out.println("Hello NormalUser"+name+"欢迎来到来到图书小练习");
        System.out.println("1.查找图书");
        System.out.println("2.借阅图书");
        System.out.println("3.归还图书");
        System.out.println("0.退出系统");
        System.out.println("----------------------------------");
        System.out.println("请输入你的操作：");
        Scanner scanner  = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;

    }

}
