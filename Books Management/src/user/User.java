package user;

import book.BookShelf;
import operate.Ioperation;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 22:08
 */
abstract public class User {
    protected String name;

    protected  Ioperation[] ioperations; //根据用户身份创建了不同的数组

    public User(String name) {

        this.name = name;
    }
     public abstract int menu();

    public void doWork(int choice, BookShelf bookShelf) {   //根据用户不同的选择进行不同的操作
        this.ioperations[choice].work(bookShelf);
    }
}
