package user;

import operate.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 22:38
 */
public class Administrator extends User {
    public Administrator(String name) {
        super(name);
        this.ioperations = new Ioperation[] {
                new ExitSystem(), //将退出系统放在数字组的0下标处，与菜单贴合
                new FindBook(),
                new AddBook(),
                new DeleteBook(),
                new ShowBook(),
        };
    }
    public int menu(){
        System.out.println("Hello Administrator"+name+"欢迎来到来到图书小练习");
        System.out.println("----------------------------------");
        System.out.println("1.查找图书");
        System.out.println("2.增加图书");
        System.out.println("3.删除图书");
        System.out.println("4.显示图书");
        System.out.println("0.退出系统");
        System.out.println("----------------------------------");
        System.out.println("请输入你的操作：");
        Scanner scanner  = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;

    }

}
