package operate;

import book.Book;
import book.BookShelf;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 22:57
 */
public class AddBook implements Ioperation{
    @Override
    public void work(BookShelf bookShelf) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入书名：");
        String name = scanner.nextLine();
        System.out.println("请输入作者：");
        String author = scanner.nextLine();
        System.out.println("请输入价格：");
        int price = scanner.nextInt();
        System.out.println("请输入类型：");
        String type = scanner.nextLine();

        Book book = new Book(name,author,price,type);
        int curSize = bookShelf.getUsedSize();
        //判断书架里是否有这本书
        for (int i = 0; i < curSize; i++) {
            Book tmp = bookShelf.getBook(i);
            if(tmp.equals(bookShelf.getBook(i).getName())){
                System.out.println("已经有了这本书，不可以再添加了！");
                return;
            }
        }

        bookShelf.setBooks(book); //如果书架没有有这本书，将增加的book存入书架
        bookShelf.setUsedSize(curSize+1); //将usedSize+1

        System.out.println("增加图书成功！");
    }

}
