package operate;

import book.Book;
import book.BookShelf;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 23:13
 */
public class BorrowBook implements Ioperation{
    @Override
    public void work(BookShelf bookShelf) {
        System.out.println("请输入你想要借阅的图书：");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int curSize = bookShelf.getUsedSize();
        for (int i = 0; i < curSize; i++) {
            if(name.equals(bookShelf.getBook(i).getName()) && !bookShelf.getBook(i).getBorrowed()) { //isBorrowed == false 简写成 ！isBorrowed）
                Book book = bookShelf.getBook(i);
                book.setBorrowed(true) ;
                System.out.println("借阅成功");
                System.out.println(book);
                return;

            }
        }
        System.out.println("未找到这本书或这本书已借出！");

    }
}
