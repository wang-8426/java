package operate;

import book.Book;
import book.BookShelf;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 23:14
 */
public class ReturnBook implements Ioperation{
    @Override
    public void work(BookShelf bookShelf) {
        System.out.println("输入你所归还的书：");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int curSize = bookShelf.getUsedSize();
        for (int i = 0; i < curSize; i++) {
            if(name.equals(bookShelf.getBook(i).getName()) && bookShelf.getBook(i).getBorrowed()){
                Book book = bookShelf.getBook(i);
                book.setBorrowed(false);
                System.out.println("归还图书成功！");
                System.out.println(book);
                return;

            }
        }
        System.out.println("输入错误，请重新输入！");

    }
}
