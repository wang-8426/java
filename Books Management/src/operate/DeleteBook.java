package operate;

import book.Book;
import book.BookShelf;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 23:05
 */
public class DeleteBook implements Ioperation{
    @Override
    public void work(BookShelf bookShelf) {
        System.out.println("请输入你要删除的书：");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int curSize = bookShelf.getUsedSize();
        for (int i = 0; i < curSize; i++) {
            if(name.equals(bookShelf.getBook(i).getName())){
                Book book = bookShelf.getBook(i);
                for (int j = i; j < curSize; j++) {
                    bookShelf.setBooks(j,bookShelf.getBook(j+1));
                }
                //System.out.println(book);
                bookShelf.setUsedSize(curSize-1);
                bookShelf.setBooks(curSize+1,null); //将最后一个多出的书置为空
                System.out.println("删除图书成功！");
                return;

            }
        }

    }
}
