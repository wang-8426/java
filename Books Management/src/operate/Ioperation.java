package operate;

import book.Book;
import book.BookShelf;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 22:49
 */

//所有对图书的操作可以抽象成为一个接口，方便使用
public interface Ioperation {
    public void work(BookShelf bookShelf);
}
