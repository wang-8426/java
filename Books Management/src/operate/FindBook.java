package operate;

import book.Book;
import book.BookShelf;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 23:00
 */
public class FindBook implements Ioperation{

    @Override
    public void work(BookShelf bookShelf) {

        System.out.println("请输入你要找的书：");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int curSize = bookShelf.getUsedSize();
        for (int i = 0; i < curSize; i++) {
            if(name.equals(bookShelf.getBook(i).getName())){
                Book book = bookShelf.getBook(i);
                System.out.println("找到了这本书！");
                System.out.println(book);
                return;

            }
        }
        System.out.println("未找到这本书！");
    }
}
