package operate;

import book.Book;
import book.BookShelf;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-18
 * Time: 23:11
 */
public class ShowBook implements Ioperation{
    @Override
    public void work(BookShelf bookShelf) {
        System.out.println("显示图书！");
        int curSize = bookShelf.getUsedSize();
        for (int i = 0; i < curSize; i++) {
            Book book = bookShelf.getBook(i);
            System.out.println(book);
        }
    }
}
