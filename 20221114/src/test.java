/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-14
 * Time: 23:12
 */
class Animal {
    protected String name;

    public Animal(String name) {
        this.name = name;
    }
}

interface IFlying {
    void fly();
}
interface IRunning {
    void run();
}
interface ISwiming {
    void swim();
}
class cat extends Animal implements IRunning {
    public cat (String name) {
        super(name);
    }
    public void run() {
        System.out.println(this.name + "正在用四条腿跑路");
    }

}
public class test {
    public static void main(String[] args) {
        Animal c = new cat("喵喵");
        run(c);
    }
}
