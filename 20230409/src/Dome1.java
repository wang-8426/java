/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-09
 * Time: 23:40
 */
class Singleton {
    //唯一实例的本体
    private static Singleton instance = new Singleton();

    //获取到实例的方法
    public static Singleton getInstance() {
        return instance;
    }

}
public class Dome1 {

    public static void main(String[] args) {
        //s1 s2 都是同一个对象
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();
    }
}
