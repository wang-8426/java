/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-09-06
 * Time: 下午 10:45
 */
class Solution {
    public int maxSubArray(int[] nums) {
        int n = nums.length;

        int res = nums[0];
        for(int i = 1;i < n;i++) {
            nums[i] += Math.max(nums[i-1],0);
            res = Math.max(res,nums[i]);
        }
        return res;

    }
}
