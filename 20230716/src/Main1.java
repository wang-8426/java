import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-16
 * Time: 下午 09:59
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        int num = str.length();
        chkParenthesis(str,num);
    }

    public static boolean chkParenthesis(String A, int n) {
        if(n == 0) {
            return true;
        }
        if(A.length()!= n) {
            return false;
        }
        char[] chars = A.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (char achar:chars) {
            if (achar == '(') {
                stack.push(achar);
            } else if (achar == ')') {
                if(!stack.empty()&&stack.peek()=='(') {
                    stack.pop();
                }else {
                    stack.push(achar);
                }
            } else {
                return false;
            }
        }
        return stack.empty();
    }
}
