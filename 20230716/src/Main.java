import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-16
 * Time: 上午 09:24
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int f1 = 0;
        int f2 = 1;
        int f3 = 0;
        while (f2<n) {
            f3 = f2+f1;
            f1 = f2;
            f2 = f3;
        }

        System.out.println(Math.min(n-f1,f2-n));
    }
}
