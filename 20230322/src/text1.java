import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-22
 * Time: 21:37
 */
public class text1 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        int count=0;
        int max=0;
        int end=0;
        for(int start=0;start<str.length();start++){
            if(str.charAt(start)>='0'&&str.charAt(start)<='9'){
                count++;
                if(max<count){//更新
                    max=count;
                    end=start;
                }
            }else{
                count=0;
            }
        }
        System.out.println(str.substring(end-max+1,end+1));
    }
}
