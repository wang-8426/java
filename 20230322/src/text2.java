import com.sun.org.apache.bcel.internal.generic.SWAP;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-22
 * Time: 22:10
 */
public class text2 {

    public static int MoreThanHalfNum_Solution(int[] numbers) {
       /* int count = 0;
        int max = 0;
        int i = 0;
        int end = 0;
        for (i = 1; i < numbers.length-1; i++) {
            if(numbers[i] == numbers[i-1]){
                count++;
                if (max < count) {
                    int temp = max;
                    max = count ;
                    count = temp;

                }
            }
            else count = 0;

        }*/

        Arrays.sort(numbers);
        int mid = numbers[numbers.length / 2];
        int j = 0;
        for (int i = 0; i < numbers.length - 1; i++) {

            {
                if (i == mid) {
                    j++;
                }
            }

        }

        return j > numbers.length / 2 ? mid : 0;
    }

    public static void main(String[] args) {
        int[] numbers = new int[10];
        Scanner scanner = new Scanner(System.in);
        int i = 0;
        while (scanner.hasNext()) {
            numbers[i] = scanner.nextInt();
            i++;
        }
        int num = MoreThanHalfNum_Solution(numbers);
        System.out.println(num);
    }
}
