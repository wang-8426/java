import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-09-13
 * Time: 下午 09:24
 */
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        int length = nums.length;
        Arrays.sort(nums);
        for (int i = 0; i < length; i++) {
            //如这个数大于0 则三数之和一定大于零
            if(nums[i] > 0) {
                break;
            }
            //去重
            if(i>0 && nums[i] == nums[i+1]) {
                continue;
            }
            int ringht = length - 1;
            int left = i + 1;
            while ((left < ringht)) {
                int sum = nums[i] + nums[left] + nums[ringht];
                if(sum == 0) {
                    result.add(Arrays.asList(nums[i],nums[left],nums[ringht]));
                    while (left < ringht && nums[left] == nums[left+1]) left++;
                    while (left < ringht && nums[ringht] == nums[ringht-1]) ringht--;
                    left++;
                    ringht--;
                }
                else if(sum < 0) left++;
                else if(sum > 0) ringht--;
            }
        }
        return result;
    }
}
