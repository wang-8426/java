/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-30
 * Time: 下午 08:10
 */
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            int count = 0;
            if(n > 0) {
                while (n != 1 ) {
                    count += n/3;
                    n = n/3 + n%3;
                    if(n == 2) {
                        n++;
                    }
                }
                System.out.println(count);
            } else {
                return;
            }

        }
    }
}
