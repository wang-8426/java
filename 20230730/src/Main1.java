import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String a = br.readLine(), b = br.readLine(), maxSub = "";
        int len = a.length(), len2 = b.length();
        if (len > len2) {
            String temp = a;
            a = b;
            b = temp;
            len = len2;
        }

        for (int i = 0; i < len; i++) {
            for (int j = i; j < len; j++) {
                String sub = a.substring(i, j + 1);
                if (!b.contains(sub)) {
                    break;
                } else {
                    if (sub.length() > maxSub.length()) {
                        maxSub = sub;
                    }
                }
            }
        }
        System.out.println(maxSub);
    }
}