import java.util.*;
/*
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-21
 * Time: 上午 11:06
 */
public class Main {
    public static void main(String[] args) {

    }


    public class LCA {
        public int getLCA(int a, int b) {
            // write code here
            while (a != b) {
                if (a > b) a /= 2;
                if (b > a) b /= 2;
            }
            return a;
        }
    }

}
