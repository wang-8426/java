
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-01
 * Time: 18:38
 */

public class Main {
    public static boolean isPrime (int number) {
        int count = 0;
        for(int i = 2; i < number; i++) {
            if((number%i) == 0) {
                count++;
            }
        }
        if(count == 0) {
            return true;
        } else {
            return false;
        }

    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int p = n / 2;
        for (int i = n/2; i < n; i++) {
            if (isPrime(i) && isPrime(n-i)) {
                System.out.println(i);
                System.out.println(n-i);
                break;
            }
        }

    }
}
