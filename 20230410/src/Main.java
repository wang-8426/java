import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-10
 * Time: 19:59
 */
public class Main {
    public static int  count (int n) {
            int x = 0;
            int y = 0;
            int count = 0;

            while (n > 0) {
                if (n > 2) {
                    x = n / 3;
                    y = n % 3;
                    n = x + y;
                    count += x;
                }
                if (n == 2) {
                    n++;
                } else if (n < 2) {
                    break;
                }
            }
            return count;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            int k = count(n);
            if(k != 0) {
                System.out.println(k);
            } else {
                break;
            }
        }
    }
}
