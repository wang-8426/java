import java.util.Scanner;
import static java.lang.Math.abs;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-20
 * Time: 10:27
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()){

            double x = sc.nextDouble();
            double y = sc.nextDouble();
            double z = sc.nextDouble();
            if(x+y >z && x+z>y && y+z>x ) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
