import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-05
 * Time: 21:25
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int m = 1;

        while (sc.hasNext()) {
            int  n = sc.nextInt();
            int count = 0;
            while(n != 0){
                if((m&n) == 1){
                    count++;
                }
                n = n>>1;
            }
            System.out.println(count);
        }
    }
}
