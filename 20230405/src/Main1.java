import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-05
 * Time: 22:27
 */
public class Main1 {
    public static int findMinimum(int n, int[] left, int[] right) {
        // write code here
        int sum = 0;//记录当前其中一个元素手套数为0，对应手套数的值
        int leftMin = Integer.MAX_VALUE;//当前手套数组的最小值，若为-就取第二小值
        int rightMin = Integer.MAX_VALUE;
        int leftSum = 0;//当前手套数组总数
        int rightSum = 0;
        for(int i = 0; i < n; i++) {
            if(left[i] * right[i] == 0) {//若当前颜色手套数为0，则表示取对应另一只手套没有意义，所以最后若随机取到需要重新在抽一次
                sum = sum + left[i] + right[i];
            } else {
                leftSum += left[i];
                if(leftMin > left[i]) {
                    leftMin = left[i];
                }
                rightSum += right[i];
                if(rightMin > right[i]) {
                    rightMin = right[i];
                }
            }
        }
        return sum + Math.min((leftSum - leftMin + 1),(rightSum - rightMin + 1)) + 1;//公式
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a1 = new int[n];
        int[] a2 = new int[n];
        int i = 0;
        int j = 0;
        while (i < 4) {
            a1[i] = sc.nextInt();
            i++;
        }
        while (j < 4) {
            a2[j] = sc.nextInt();
            j++;
        }
        int l = findMinimum(n,a1,a2);
        System.out.println(l);
    }

}
