import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-28
 * Time: 下午 08:52
 */
import java.util.*;
public class Main{
    public static void dfs(int i, int n, int m, ArrayList<Integer> list){
        if(m == 0){
            for(int j = 0; j < list.size(); j++){
                if(j != list.size()-1){
                    System.out.print(list.get(j) + " ");
                }
                else{
                    System.out.println(list.get(j));
                }
            }
        }
        else{
            for(int j = i+1; j <= n; j++){
                list.add(j);
                dfs(j, n, m-j, list);
                list.remove(list.size()-1);
            }
        }
    }
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        ArrayList<Integer> list = new ArrayList<>();
        for(int i = 1; i <= n; i++){
            list.add(i);
            dfs(i, n, m-i, list);
            list.remove(list.size()-1);
        }
    }
}
