/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-25
 * Time: 23:25
 */
public class PassWordException extends RuntimeException {
    public PassWordException(String s) {
        super(s);
    }
}
