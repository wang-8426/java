/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-25
 * Time: 23:24
 */
public class UserNameException extends RuntimeException {

    public UserNameException(String s) {
        super(s);
    }

}
