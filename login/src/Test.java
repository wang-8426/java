/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-25
 * Time: 23:20
 */
public class Test {
    public String name = "bobo";
    public String password = "123456";

    public void login(String name,String password) throws UserNameException {
        if(!this.name.equals(name)) {
            throw new UserNameException("你输入的用户名错误！");
        }

        if (!this.password.equals(password)) {
            throw new PassWordException("您输入的密码错误");
        }

    }

    public static void main(String[] args) { //无法将用户名异常和密码异常同时检测出来
        Test test = new Test();
        try {
            test.login("obo","23456");
        } catch (UserNameException e) {
            e.printStackTrace();
        } catch (PassWordException e) {
            e.printStackTrace();
        }
        finally {

        }
    }
}
