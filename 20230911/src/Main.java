/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-09-11
 * Time: 下午 10:36
 */
class Solution {
    public void moveZeroes(int[] nums) {
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] != 0) {
                nums[k] = nums[i];
                k++;
            }

        }
        while (k!= nums.length) {
            nums[k] = 0;
            k++;
        }
    }
}
