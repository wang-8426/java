import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-28
 * Time: 21:31
 */
public class Main {
    public static boolean isSortLength(String[] arr){
        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i].length()>arr[i+1].length()){
                return false;
            }
        }
        return true;
    }
    public static boolean isSortZiDian(String[] arr){
        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i].compareTo(arr[i+1]) >0){
                return false;
            }
        }
        return true;
    }
         public static void main(String[] args) throws IOException {
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
             int n = Integer.parseInt(bufferedReader.readLine());
             String[] arr = new String[n];
             for (int i = 0; i < n; i++) {
                 arr[i] = bufferedReader.readLine();
             }
             if (isSortLength(arr)&&isSortZiDian(arr)){
                 System.out.println("both");
             }
             else if (isSortZiDian(arr)){
                 System.out.println("Lexicographically");
             }else if (isSortLength(arr)){
                 System.out.println("Lengths");
             }
             else {
                 System.out.println("none");
             }
         }
}
