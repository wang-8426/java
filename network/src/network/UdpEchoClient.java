package network;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;
import java.util.SortedMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-07
 * Time: 19:30
 */
public class UdpEchoClient {
    private DatagramSocket socket = null;
    private String serverIp;
    private int serverport;

    //客户端启动需要知道服务器在哪
    public UdpEchoClient(String serverIp,int serverPort) throws SocketException {
        socket = new DatagramSocket();
        this.serverIp = serverIp;
        this.serverport = serverPort;
    }

    public void start() throws IOException {
        Scanner scanner = new Scanner(System.in);
        //通过这个客户端可以与服务器多次交互
        while (true) {
            //1. 从控制台读取一串字符
            //   打印一个提示符，提示用户输入内容
            System.out.println("-> ");
            String request = scanner.next();
            //2.把字符串构造成udp packet，并进行发送
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(),request.getBytes().length,
                    InetAddress.getByName(serverIp),serverport);

            socket.send(requestPacket);
            //3.客户端尝试读取服务器返回的响应
            DatagramPacket responsePacket = new DatagramPacket(new  byte[4096],4096);
            socket.receive(responsePacket);
            //4.把响应的数据转换成String显示出来、
            String response = new String(responsePacket.getData(),0,responsePacket.getLength());
            System.out.printf("req: %s,resp: %s \n",request,response);
        }
    }

    public static void main(String[] args) throws IOException {
        UdpEchoClient udpEchoClient = new UdpEchoClient("127.0.0.1",9090);
        udpEchoClient.start();
    }

}
