package network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-07
 * Time: 19:31
 */
public class UdpEchoServer {
    //先定义一个socket对象
    private DatagramSocket socket = null;
    //当某个端口被别的进程占用了，会绑定出错
    public UdpEchoServer(int port) throws SocketException {
        //构造socket同时绑定端口
        socket = new DatagramSocket(port);
    }
    //启动服务器主逻辑
    public void start() throws IOException {
        System.out.println("服务器启动！");
        while (true) {
            //每次循环干三件事：
            //1.读取请求并解析
            DatagramPacket requestPacket = new DatagramPacket(new byte[4096],4096);

            socket.receive(requestPacket);
            //为了方便处理请求将数据包转成String
            String request = new String(requestPacket.getData(),0, requestPacket.getLength());
            //2.根据请求计算响应
            String respons = process(request);
            //写的是回显服务器所以process方法是返回请求的内容

            //3.把结果反馈给客户端
            //  根据respons构造一个DatagramPacket
            //  构造请求时要指定这个包发给谁
            DatagramPacket responsePacket = new DatagramPacket(respons.getBytes(),respons.getBytes().length,
                    requestPacket.getSocketAddress());
            socket.send(responsePacket);
        }


    }
    public String process(String request){
        return  request;
    }

    public static void main(String[] args) throws IOException {
        UdpEchoServer udpEchoSever = new UdpEchoServer(9090);
        udpEchoSever.start();
    }
}
