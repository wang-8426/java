package network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-08
 * Time: 11:18
 */
public class TcpEchoServer {
    private ServerSocket serverSocket = null;

    public TcpEchoServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }
    public void start() throws IOException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        System.out.println("服务器启动");
        while (true) {
            Socket clientsocket = serverSocket.accept();
            //直接调用该方法会影响这个循环二次执行，导致accept不及时
            //每日次客户端发来请求都创建新线程，用新线程调用processConnection

         /*   Thread t = new Thread(()->{
                try {
                processConnection(clientsocket);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            t.start();*/
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        processConnection(clientsocket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
    //通过这个方法处理连接

    private void processConnection(Socket clientsocket) throws IOException {
        System.out.printf("[%s:%d] 客户端上线！\n",clientsocket.getInetAddress().toString(),
                clientsocket.getPort());

        try (InputStream inputStream = clientsocket.getInputStream();
             OutputStream outputStream = clientsocket.getOutputStream()){
            //将字节流包装成字符流
            Scanner scanner = new Scanner(inputStream);
            PrintWriter printWriter = new PrintWriter(outputStream);
            while (true) {
                //1.读取请求
                if (!scanner.hasNext()) {
                    //读取数据库到结尾了，可以关闭
                    System.out.printf("[%s:%d] 客户端下线\n ",clientsocket.getInetAddress().toString(),
                            clientsocket.getPort());
                    break;
                }
                //直接用scanner读取一段字符串
                String request = scanner.next();

                //2.根据请求计算响应
                String response = process(request);
                //3.把相应返回个客户端
                printWriter.println(response);
                //刷新缓冲区
                printWriter.flush();
                System.out.printf("[%s:%d] req: %s; resp: %s\n",clientsocket.getInetAddress().toString(),
                        clientsocket.getPort(),request,response);

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            clientsocket.close();
        }


    }

    private String process(String request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        TcpEchoServer tcpEchoServer = new TcpEchoServer(9090);
        tcpEchoServer.start();
    }
}


