import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-18
 * Time: 下午 08:41
 */
public class Main {
    public static boolean isDictSort (String[] strs) {
        for (int i = 0; i < strs.length-1; i++) {
            if(strs[i].compareTo(strs[i+1]) > 0) {
                return false;
            }
        }
        return true;
    }
    public static boolean isLenSort (String[] strs) {
        for (int i = 0; i < strs.length-1; i++) {
            if (strs[i].length()>strs[i+1].length()) {
                return false;
            }
        }
        return true;

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n  = sc.nextInt();
        String[] strings = new String[n];
        int i = 0;
        while (i < n) {
            strings[i] = sc.next();
            i++;
        }
        boolean dict = isDictSort(strings);
        boolean len = isLenSort(strings);
        if(dict && len) {
            System.out.println("both");
        } else if (dict){
            System.out.println("lexicographically");
        } else if (len) {
            System.out.println("lengths");
        } else {
            System.out.println("none");
        }
    }
}
