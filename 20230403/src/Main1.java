import java.util.*;
public class Main1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();

        int[] step = new int[m + 1];
        for(int i = 0;i < m + 1;i++){
            step[i] = Integer.MAX_VALUE;
        }

        step[n] = 0;
        for(int i = n;i < m + 1;i++){
            //无法到达的点
            if(step[i] == Integer.MAX_VALUE){
                continue;
            }
            List<Integer> list = div(i);
            for(int j : list){
                //没有到达终点并且之前到达过
                if(i + j < m + 1&&step[i + j] != Integer.MAX_VALUE){
                    step[i + j] = Math.min(step[i + j],step[i] + 1);
                }
                //没有到达终点但之前没到达过
                else if(i + j < m + 1){
                    step[i + j] = step[i] + 1;
                }
            }
        }

        if(step[m] != Integer.MAX_VALUE){
            System.out.println(step[m]);
        }else{
            System.out.println(-1);
        }
    }

    public static List<Integer> div(int n){//返回约数合集，需要减小时间复杂度
        List<Integer> list = new ArrayList<Integer>();
        for(int i = 2;i * i <= n;i++){
            if(n % i == 0){
                list.add(i);
                if(n / i != i){
                    list.add(n / i);
                }
            }
        }
        return list;
    }
}