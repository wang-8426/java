import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        int count=0;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='"'){
                do{
                    i++;
                }while(str.charAt(i)!='"');
            }
            if(str.charAt(i)==' '){
                count++;
            }
        }
        System.out.println(count+1);
        int flag=1;
        for(int i=0;i<str.length();i++){
//遇到第一个双引号，flag变为-1，
//遇到第二个双引号结束后flag重新变为1
//只要在打印双引号中的内容的时候flag的值始终为0
            if(str.charAt(i)=='"'){
                flag = -flag;
            }
            //除了双引号和特殊空格以外的字符都要打印
            if(str.charAt(i)!=' '&&str.charAt(i)!='"'){
                System.out.print(str.charAt(i));
            }
//双引号中的空格需要打印
            if(str.charAt(i)==' '&&flag==-1){
                System.out.print(str.charAt(i));
            }
//双引号外碰到空格，需要换行
            if(str.charAt(i)==' '&&flag==1){
                System.out.println();
            }
        }
    }
}
