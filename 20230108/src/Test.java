/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-01-08
 * Time: 20:09
 */
public class Test {
    public static void main(String[] args) {
        MyBinaryTree myBinaryTree = new MyBinaryTree();
        MyBinaryTree.TreeNode root = myBinaryTree.createTree();
        myBinaryTree.preOrder(root);
        System.out.println("==========");
        //MyBinaryTree.nodeSize = myBinaryTree.size(root);
        //myBinaryTree.size2(root);
        //System.out.println(MyBinaryTree.nodeSize);
        System.out.println(myBinaryTree.getLeafNodeCount1(root));
        System.out.println("===========");
        myBinaryTree.getLeafNodeCount2(root);
        System.out.println("叶子节点个数"+MyBinaryTree.leafSize);
        System.out.println("===========");
        System.out.println(myBinaryTree.getKLevelNodeCount(root,3));
        System.out.println("===========");
        System.out.println(myBinaryTree.getHeight(root));
        System.out.println("===========");
        System.out.println(myBinaryTree.find(root,'C').val+' ');
        System.out.println("===========");
        myBinaryTree.levelOrder(root);
        System.out.println("===========");
        System.out.println(myBinaryTree.isCompleteTree(root));

    }
}
