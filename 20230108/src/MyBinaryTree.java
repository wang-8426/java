import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-01-08
 * Time: 20:08
 */
public class MyBinaryTree {

    static class TreeNode {
        public char val;
        public TreeNode left;//左孩子的引用
        public TreeNode right;//右孩子的引用

        public TreeNode(char val) {
            this.val = val;
        }

    }


    /**
     * 创建一棵二叉树 返回这棵树的根节点
     *
     * @return
     */
    public TreeNode root;

    public TreeNode createTree() {
        TreeNode A = new TreeNode('A');
        TreeNode B = new TreeNode('B');
        TreeNode C = new TreeNode('C');
        TreeNode D = new TreeNode('D');
        TreeNode E = new TreeNode('E');
        TreeNode F = new TreeNode('F');
        TreeNode G = new TreeNode('G');
        TreeNode H = new TreeNode('H');
        A.left = B;
        A.right = C;
        B.left = D;
        B.right = E;
        C.left = F;
        C.right = G;
        E.right = H;
        //this.root = A;
        return A;

    }

    // 前序遍历
    public void preOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        System.out.print(root.val + "  ");
        preOrder(root.left);
        preOrder(root.right);
    }

    // 中序遍历
    void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        preOrder(root.left);
        System.out.print(root.val + " ");
        preOrder(root.right);
    }

    // 后序遍历
    void postOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        preOrder(root.left);
        preOrder(root.right);
        System.out.print(root.val + " ");
    }

    public static int nodeSize;

    /**
     * 获取树中节点的个数：遍历思路
     */
    /*public int size(TreeNode root) {
       if (root == null) {
           return 0;
       }
       int leftSize = size(root.left);
       int rightSize = size(root.left);
       return leftSize + rightSize + 1;
    }

    /**
     * 获取节点的个数：子问题的思路
     *
     * @param root
     * @return
     */
    void size2(TreeNode root) {
        if (root == null) {
            return;
        }
        nodeSize++;
        size2(root.left);
        size2(root.right);
    }


    /*
     获取叶子节点的个数：遍历思路
     */
    public static int leafSize = 0;

    int getLeafNodeCount1(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        int getleftsize = getLeafNodeCount1(root.left);
        int getrightsize = getLeafNodeCount1(root.right);
        return getleftsize + getrightsize;

    }


    /*
     获取叶子节点的个数：子问题
     */
    void getLeafNodeCount2(TreeNode root) {
        if (root == null) {
            return;
        }
        if (root.left == null && root.right == null) {
            leafSize++;
        }
        getLeafNodeCount2(root.left);
        getLeafNodeCount2(root.right);

    }


    /*
    获取第K层节点的个数
     */
    int getKLevelNodeCount(TreeNode root, int k) {
        if(root == null) {
            return 0;
        }
        if(k == 1) {
            return 1;
        }
        int leftcount = getKLevelNodeCount(root.left,k-1);
        int rightcount = getKLevelNodeCount(root.right,k-1);
        return leftcount+rightcount;

    }

    /*
     获取二叉树的高度
     时间复杂度：O(N)
     */
    public int treeHight;
   int getHeight(TreeNode root) {
       if(root == null) {
           return 0;
       }
       if(root.left == null&& root.right == null) {
           return 1;
       }
       return getHeight(root.left) > getHeight(root.right) ?
               getHeight(root.left)+1: getHeight(root.right)+1;
    }


    // 检测值为value的元素是否存在
    TreeNode find(TreeNode root, int val) {
        if(root == null) {
            return null;
        }
        if(root.val == val) {
            return root;
        }
        TreeNode leftTree = find(root.left,val);
        if(leftTree != null) {
            return leftTree;
        }
        TreeNode rightTree = find(root.right,val);
        if(rightTree != null) {
            return rightTree;
        }
        return null;//没有找到
    }

    //层序遍历
    void levelOrder(TreeNode root) {
       if(root == null){
           return;
       }
        Queue<TreeNode> queue = new LinkedList<>();
       queue.offer(root);
       while(!queue.isEmpty()){
               TreeNode cur = queue.poll();
               System.out.print(cur.val+" ");
           if(cur.left != null) {
               queue.offer(cur.left);
           }
           if(cur.right != null) {
               queue.offer(cur.right);
           }
       }

    }


    // 判断一棵树是不是完全二叉树
    public  boolean isCompleteTree(TreeNode root)
    {
        if(root==null) return false;
        Queue<TreeNode> queue=new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty())
        {
            TreeNode cur=queue.poll();
            if(cur==null)
            {
                break;
            }else{
                if(cur.left!=null) queue.offer(cur.left);
                if(cur.right!=null) queue.offer(cur.right);
            }
        }
        //判断队列中剩余的元素是否为空
        while(!queue.isEmpty())
        {
            TreeNode cur=queue.poll();
            if(cur!=null)  return false;
        }
        return true;
    }


}
