import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-06
 * Time: 9:31
 */
public class test  {
    //冒泡排序
    public static void main1(String[] args) {
        int[] array1 = {1,4,5,2,8,5,7};
        for (int i = 0; i < array1.length - 1; i++) {
            for (int j = 0; j < array1.length - 1 -i; j++) {
                if (array1[j] > array1[j+1]) {
                    int tmp = 0;
                    tmp = array1[j];
                    array1[j] = array1[j+1];
                    array1[j+1] = tmp;
                }
            }
        }
        System.out.println(Arrays.toString(array1));
    }

    //两数之和
    public static int[] findSum(int[] array,int n) {
        int[] array2 = {-1,-1};
        for (int i = 0; i < array.length ; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if(array[i] + array[j] == n) {
                    array2[0] = i;
                    array2[1] = j;
                    return array2;
                }
            }
        }
        return array2;
    }
    public static void main2(String[] args) {
        int[] array1 = {1,2,3,4,6,9,13};
        System.out.println(Arrays.toString(findSum(array1,14)));
    }

    //查找只出现一次的数字
    public static int findNum (int[] array) {
        int ret = array[0];
        for (int i = 1; i < array.length; i++) {
            ret ^= array[i];
        }
        return ret;
    }
    public static void main3(String[] args) {
        int[] array1 = {1,1,3,4,3};
        System.out.println(findNum(array1));
    }

    //出现次数 大于 ⌊ n/2 ⌋ 的元素
    public static int findVal(int[] array) {
        return array[array.length/2];
    }
    public static void main4(String[] args) {
        int[] array1 = {1,2,3,3,3,3,4};
        System.out.println(findVal(array1));
    }

    //统计是否有三个奇数连续出现
    public static boolean func(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if(array[i] % 2 == 1) {
                count++;
                if(count == 3) {
                    return true;
                }
            } else {
                count = 0;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        int[] array1 = {1,2,3,4,5,7,9,10};
        System.out.println(func(array1));
    }

}
