import java.util.Scanner;

class Node {
    int val;
    Node next;

    public Node() {
    }

    public Node(int val) {
        this.val = val;
    }
}
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();//单链表的长度
        sc.nextLine();
        String str = sc.nextLine();
        String[] nodes = str.split(" ");
        Node newHead = new Node(-1);
        Node tail = newHead;
        for (int i = 0; i < nodes.length; i++) {
            Node node = new Node(Integer.parseInt(nodes[i]));
            tail.next = node;
            tail = node;
        }
        String part = sc.nextLine();
        int left = Integer.parseInt(part.split(" ")[0]);
        int right = Integer.parseInt(part.split(" ")[1]);
        //进行反转
        Node head = reverseNode(newHead.next,left,right);
        while (head != null) {
            System.out.print(head.val+" ");
            head = head.next;
        }
    }
    //头插法
    public static Node reverseNode(Node head,int left,int right) {
        Node newHead = new Node(-1);
        newHead.next = head;
        Node pre = newHead;//前驱节点
        for (int i = 1; i < left; i++) {
            pre = pre.next;
        }
        Node cur = pre.next;//要反转的第一个节点
        for (int i = 0; i < (right-left); i++) {
            Node curNext = cur.next;
            cur.next = curNext.next;
            //头插法
            curNext.next = pre.next;
            pre.next = curNext;
        }
        return newHead.next;
    }
}
