import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-26
 * Time: 23:06
 */
public class Text {
    private static int fib(int n) {
        if(n == 0) {
            return 0;
        }
        if(n == 1 || n == 2) {
            return 1;
        }
        return fib(n-1)+fib(n-2);
    }
    //斐波那契数列
    public static void main(String[] args) {
        int f0 = 0;
        int f1 = 1;
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int i = 0;
        for ( i = 0; i <= N; i++) {
            if (fib(i)>N) {
                break;
            }
        }
        int x = Math.abs(fib(i)-N);
        int y = Math.abs(fib(i-1)-N);
        int z = x > y ? y : x;
        System.out.println(z);
    }

}
