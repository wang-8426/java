import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-28
 * Time: 下午 05:39
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] triangle = new int[n+1][2*n];
        triangle[1][1] = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= 2*n-1; j++) {
                for (int k = j-2; k <= j ; k++) {
                    if (k<1 || k>(2*(i-1)-1)){
                        continue;
                    }
                    triangle[i][j] += triangle[i-1][k];
                }
            }
        }
        int ans = -1;
        for (int m = 1; m < 2*n-1; m++) {
            if(triangle[n][m]%2 == 0) {
                ans = m;
                break;
            }
        }
        System.out.println(ans);
    }
}
