import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-01
 * Time: 13:49
 */
public class test {
    //分数求和
    public static void main1(String[] args) {
        double sum = 0;
        int flag = 1;
        double t = 0 ;
        for (int i = 1; i <= 100; i++) {
            t = 1.0/i;
            sum += flag * t;
            flag = -flag;
        }
        System.out.println(sum);
    }

    public static void main2(String[] args) {
        //提出数字的每一位
        int a = 123;
        int bity = 0;
        while(a != 0){
            bity = a % 10;
            a = a / 10;
            System.out.print(bity+" ");
        }
    }

    public static void main(String[] args) {
        //99 乘法表
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i+"*"+j+"="+(i*j)+" ");
            }
            System.out.println();
        }
    }
}
