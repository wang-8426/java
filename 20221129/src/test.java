/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-29
 * Time: 21:18
 */
import java.util.Scanner;

 class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextDouble()) {
            double a = in.nextDouble();
            double ret = 0;
            ret = a > (int)a+0.4 ? (int)a+1 : (int) a;
            System.out.println((int)ret);
        }
    }
}
