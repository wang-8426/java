import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-23
 * Time: 19:38
 */
public class Main {
        public static void main(String[] args) {
                int x, y, z, r;
                int A, B, C;
                Scanner sc = new Scanner(System.in);
                x = sc.nextInt();
                y = sc.nextInt();
                z = sc.nextInt();
                r = sc.nextInt();
                int k = (y + r)/2;
                A = (x + z)/2;
                B = k;
                C = r - k;
                if ((A >= -30 && A <= 30)&&(B >= -30 && B <= 30)&&(C >= -30 && C <= 30)) {
                        System.out.print(A);
                        System.out.print(' ');
                        System.out.print(B);
                        System.out.print(' ');
                        System.out.print(C);
                }
                else {
                        System.out.println("No");
                }

        }
}
