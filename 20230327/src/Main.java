import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-27
 * Time: 20:46
 */
public class Main {
    public static boolean chkParenthesis(String str ){
        //待匹配的左括号数量
        int left = 0;
        for (int i = 0; i < str.length()-1; i++) {

        char c = str.charAt(i);
                if(c == '('){
                    left++;
                }else{
                    //遇到右括号
                    left--;
                }
                if(left <0){
                    return false;
                }
        }
        return left == 0;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        //
        // int n = sc.nextInt();
        boolean ret = chkParenthesis(str);
        System.out.println(ret);

    }
}

