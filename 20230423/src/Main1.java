import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-22
 * Time: 16:41
 */



public class Main1 {
    public static void main(String[] args) {
        //进行输入操作
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            if (isPrimer(n)) {
                System.out.println(1);
                continue;
            }
            int count = 0;
            //从i=2开始到sqrt(num)做循环,先要保证这个数字是质数
            for (int i = 2; i <= Math.sqrt(n); i++) {
                //在i是质数的基础上,要保证i是另一个数的因数
                if (n % i == 0) {
                    while (n % i == 0) {
                        n /= i;
                    }
                    count++;  //count++这步必须保证是在是双重条件的基础上
                }
            }
            //质数是不包含1的
            if (n != 1) {
                count++;
            }
            System.out.println(count);
        }
    }

    public static boolean isPrimer(int n) {
        for (int i = 2; i <= Math.pow(n, 0.5); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}