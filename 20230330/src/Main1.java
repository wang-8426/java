import java.util.*;
public class Main1{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        while(scan.hasNext()){
            String str = scan.nextLine();

            int sum1 = getLength(str);//长度

            int sum2 = getChar(str);//字母

            int sum3 = getNum(str);//数字

            int sum4 = getSym(str);//符号

            //最后的分数
            int sum = 0;
            if(sum2 == 20 && sum3 >= 1 && sum4 >= 1){
                sum = sum1 + sum2 + sum3 + sum4 + 5;
            }else if(sum2==10&&sum3>=1&&sum4>=1){
                sum=sum1+sum2+sum3+sum4+3;
            }else if(sum2==10&&sum3>=1&&sum4==0){
                sum=sum1+sum2+sum3+sum4+2;
            }else{
                sum=sum1+sum2+sum3+sum4;
            } if(sum>=90){
                System.out.println("VERY_SECURE");
            }else if(sum>=80){
                System.out.println("SECURE");
            }else if(sum>=70){
                System.out.println("VERY_STRONG");
            }else if(sum>=60){
                System.out.println("STRONG");
            }else if(sum>=50){
                System.out.println("AVERAGE");
            }else if(sum>=25){
                System.out.println("WEAK");
            }else if(sum>=0){
                System.out.println("VERY_WEAK");
            }
        }
    }

    public static int getLength(String str){
        if(str.length() <= 4){
            return 5;
        }else if(7 >= str.length() && str.length() >= 5){
            return 10;
        }else{
            return 25;
        }

    }
    public static int getChar(String str){
        int small = 0;
        int big = 0;
        for(int i = 0;i < str.length();i++){
            if(str.charAt(i) >= 65 && str.charAt(i) <= 90){
                big++;
            }else if(str.charAt(i) >= 97 && str.charAt(i) <= 122){
                small++;
            }
        }
        if(small > 0 && big > 0){
            return 20;
        }else if(small > 0 || big > 0){
            return 10;
        }else{
            return 0;
        }
    }

    public static int getNum(String str){
        int num = 0;
        for(int i = 0;i < str.length();i++){
            if(str.charAt(i) - '0' >= 0 && str.charAt(i) - '0' <= 9){
                num++;
            }
        }
        if(num > 1){
            return 20;
        }else if(num == 1){
            return 10;
        }else{
            return 0;
        }
    }

    public static int getSym(String str){
        int num = 0;
        for(int i = 0;i < str.length();i++){
            if(!(str.charAt(i) >= 65 && str.charAt(i) <= 90) && !(str.charAt(i) >= 97 && str.charAt(i) <= 122)
                    && !(str.charAt(i) - '0' >= 0 && str.charAt(i) - '0' <= 9)){
                num++;
            }
        }
        if(num > 1){
            return 25;
        }else if(num == 1){
            return 10;
        }else{
            return 0;
        }

    }
}
