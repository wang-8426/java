import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-30
 * Time: 21:39
 */
public class Main {
    public static boolean judge(int[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ((board[i][0] + board[i][1] + board[i][2] == 3)||(board[0][j] + board[1][j] + board[2][j] == 3)) {
                    return true;
                }
                if ((board[0][0] + board[1][1] + board[2][2] == 3)||(board[2][0] + board[1][1] + board[0][2] == 3)) {
                    return true;
                }
            }
        }
        return false;
    }



    public static void main(String[] args) {
        int[][] board = new int[3][3];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = sc.nextInt();

            }
        }
        System.out.println(judge(board));

    }
}
