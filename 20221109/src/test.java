/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-09
 * Time: 22:58
 */
public class test {
    public static void main(String[] args) {
        //数字转字符串 String -> java中的字符串类型
//        int a = 165165;
//        String ret = String.valueOf(a);
//        System.out.println(ret);

        //字符串转数字 包装类型封装的valueOf 可以用来讲字符串中的数字提出
//        String a="16564";
//        int ret =Integer.valueOf(a);
//        System.out.println(ret);

        //java中的字符串可以相加，类似strcat
//        String a="Hello";
//        String b="World";
//        System.out.println(a+b);

        //将字符串和数字相加，将得到字符串 从左往右依次运算
//        int a=10;
//        int b=20;
//        String ret="hello";
//        System.out.println("a的值是"+a+b);
//        System.out.println("a的值是"+(a+b));
//        System.out.println(a+b+"a的值是");

        //按位与 & 两个数字二进制某一位都为1，则这一位是1
        // 按位或 | 两个数字二进制某一位有一个是1，则这一位是1
        // 按位异或^ 两个数字二进制某一位都相同（都是0或都是1），则这一位是1，否则为零





    }
}
