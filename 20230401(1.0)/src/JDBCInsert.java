import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-01
 * Time: 22:10
 */
public class JDBCInsert {

        public static void main(String[] args) throws SQLException {
            //1.创建并初始化一个数据源
            DataSource dataSource = new MysqlDataSource();
            ((MysqlDataSource) dataSource).setURL("jdbc:mysql://127.0.0.1:3306/s2023?characterEncoding=utf8&useSSl=false");
            ((MysqlDataSource) dataSource).setUser("root");
            ((MysqlDataSource) dataSource).setPassword("9264");
            //2.与数据库建立链接
            Connection connection = dataSource.getConnection();
            //3.读取用户输入内容
            Scanner sc = new Scanner(System.in);
            //请输入姓名
            String name = sc.nextLine() ;
            //请输入学号
            int id= sc.nextInt();
            //构造sql语句
            String sql = "insert into user value (?,?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1,id);
            statement.setString(2,name);
            //4.执行sql语句
            int ret = statement.executeUpdate();
            System.out.println("ret = " + ret );
            //释放资源
            statement.close();
            connection.close();
        }
    }

