import com.sun.org.apache.xerces.internal.impl.xs.SchemaNamespaceSupport;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-08
 * Time: 11:14
 */
public class Main {
    public static int f(int n) {

        if (n == 1 || n == 2)

            return 1;

        else

            return f(n - 1) + f(n - 2);

    }
    public static void main(String[] args) {
        Scanner sc  = new Scanner(System.in);
        int n =  sc.nextInt();
        System.out.println(f(n));
    }
}
