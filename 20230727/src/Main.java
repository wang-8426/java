import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-27
 * Time: 下午 09:56
 */

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int  n = sc.nextInt();
        int count = 0;
        for (int i = 1; i < n; i++) {
            count+=isComplete(i);
        }
        System.out.println(count);

    }
        public static int isComplete (int n) {
            int count = 0;
            for (int i = 1; i < n; i++) {
                if (n % i == 0) {
                    count += i;
                }
            }
            if (count == n) {
                return 1;
            } else {
                return 0;
            }
        }
}
