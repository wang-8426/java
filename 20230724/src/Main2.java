/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-24
 * Time: 下午 08:21
 */
import java.util.*;
public class Main2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String s = sc.nextLine();
            StringBuilder sb = new StringBuilder();
            ArrayList<String> list = new ArrayList();
            boolean flag = false;
            for(int i=0;i<s.length();i++){
                char c = s.charAt(i);
                if(c=='"'){
                    //遇到第一个引号 flag为true,第二个引号 flag为false
                    flag = flag?false:true;
                    continue;//继续遍历下一个字符
                }
                //如果c是空格 ,且flag为false时,即没有引号 或已经是第二个引号结束
                if(c==' ' && !flag){
                    list.add(sb.toString());//往集合中添加当前拼接到的字符串
                    sb = new StringBuilder();//置空  重新遍历下一个字符
                }else{
                    //即 不是引号 也不是第二种逻辑
                    sb.append(c);
                }
            }
            //最后遍历结束 没有空格或引号,需要再加上sb 打印出数组的长度,即几个命令
            list.add(sb.toString());
            System.out.println(list.size());
            //打印出命令
            for(String s1 : list){
                System.out.println(s1);
            }
        }
    }
}
