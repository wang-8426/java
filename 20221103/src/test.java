import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-03
 * Time: 7:05
 */
public class test {
//求一个数每一位的相加的和
    public static int fun(int n) {
        if(n < 10) {
            return n;
        }
        return n%10 + fun(n/10);
    }
    public static void main1(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        System.out.println(fun(a));
    }

    //斐波那契数列
    public static int fab(int n) {
        if(n == 1||n==2) {
            return 1;
        }
        return fab(n-1)+fab(n-2);
    }
    public static void main2(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        System.out.println("斐波那契数列第"+a+"项为"+fab(a));
    }

    //汉诺塔递归
    public static void hanoiTower(int n,char pos1,char pos2,char pos3) {
        if(n == 1) {
            move(pos1,pos3);
            return;
        }
        hanoiTower(n-1,pos1,pos3,pos2);
        move(pos1,pos2);
        hanoiTower(n-1,pos2,pos3,pos1);
    }
    public static void move(char pos1,char pos2) {
        System.out.print(pos1 +"->"+pos2+" ");
    }
    public static void main3(String[] args) {
        hanoiTower(3 , 'A', 'B', 'c');
    }
}
