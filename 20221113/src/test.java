/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-13
 * Time: 20:05
 */
class B {
    public int Func() {
        System.out.print("B");
        return 0;
    }
}
class D extends B {
    @Override
    public int Func() {
        System.out.print("D");
        return 0;
    }
}
public class test {
    public static void main1(String[] args) {
        B a = new B();
        B b = new D();
        a.Func();
        b.Func();
    }
}
 class Person{

    private String name = "Person";

    int age=0;

}

public class Child extends Person{

    public String grade;

    public static void main(String[] args){

        Person p = new Child();

        System.out.println(p.name);

    }

}