import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-10-31
 * Time: 22:10
 */
public class test {
    //统计二进制数字中1的个数
    public static void main1(String[] args) {
        Scanner scan = new Scanner(System.in);
        int count = 0;
        int  a = scan.nextInt();
        while(a != 0){
            if((a & 1)==1){
                count++;
            }
            a = a >>> 1;
        }
        System.out.println(count);
    }

    public static void main2(String[] args) {
        //求最大公约数
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int tmp = 0;
        //确保a > b
        if(a < b){

            tmp = a;
            a = b;
            b = tmp;
        }
        //辗转相除法
        int c = a % b;
        while(c != 0){
            b = a;
            a = c;
            c = a % b;
        }
        System.out.println(a);

    }

    public static void main(String[] args) {
        //输出x型图案
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                if(i == j||i + j == a-1) {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
                System.out.println();
        }
    }

}
