/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-15
 * Time: 22:12
 */
class Base1 {
    public Base1() {
        System.out.println("Base()");
    }
}
class Derived1 extends Base1 {
        public Derived1() {
            super();
        // 注意子类构造方法中默认会调用基类的无参构造方法：super(),
        // 用户没有写时,编译器会自动添加，而且super()必须是子类构造方法中第一条语句，
        // 并且只能出现一次
            System.out.println("Derived1()");
        }
}
    public class Test2 {
        public static void main(String[] args) {
            Derived1 d = new Derived1();
        }
}