/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-15
 * Time: 20:40
 */
//class Dog {
//        String name;
//        int age;
//        float weight;
//
//        public void eat(){
//            System.out.println(name + "正在吃饭");
//        }
//
//        public void sleep(){
//            System.out.println(name + "正在睡觉");
//        }
//
//        void Bark(){
//            System.out.println("汪汪叫！");
//        }
//}
//class Cat{
//    String name;
//    int age;
//    float weight;
//
//    public void eat(){
//        System.out.println(name + "正在吃饭");
//    }
//
//    public void sleep()
//    {
//        System.out.println(name + "正在睡觉");
//    }
//
//    void mew(){
//        System.out.println(name + "喵喵喵~~~");
//    }
//}
class Animal {
    String name;
    int age;
    float weight;
    public void eat(){
        System.out.println(name+"在吃饭！");
    }
    public void sleep() {
        System.out.println(name+"在睡觉！");
    }

}
class Dog extends Animal {
    public void bark() {
        System.out.println(name+"汪汪叫！");
    }
}
class Cat extends Animal{
    public void miao(){
        System.out.println(name+"喵喵叫！");
    }
}
public class Test {
    public static void main1(String[] args) {
        Dog dog = new Dog();
        // dog类中并没有定义任何成员变量，name和age属性肯定是从父类Animal中继承下来
        System.out.println(dog.name);
        System.out.println(dog.age);
        // dog访问的eat()和sleep()方法也是从Animal中继承下来的
        dog.eat();
        dog.sleep();
        dog.bark();
    }
}
