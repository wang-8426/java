import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-21
 * Time: 19:35
 */
public class text {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int[] A = new int[n];
        int i;
        for (i = 0; i < n; i++){
            A[i] = sc.nextInt();
        }
        int flag = 0;
        int count = 1;
        for (i = 1; i < n; i++) {
            if (A[i] > A[i - 1]) {
                if (flag == 0) flag = 1;
                else if (flag == -1) {
                    count++;
                    flag = 0;
                }
            } else if (A[i] < A[i - 1]) {
                if (flag == 0) flag = -1;
                else if (flag == 1) {
                    flag = 0;
                    count++;
                }
            }

        }
        System.out.println(count);

    }
}
