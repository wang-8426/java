import java.util.Scanner;
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-21
 * Time: 19:57
 */

    public class text1{
        public static int sonCount(int[] A, int n) {
            int flag = 0;// 递减为-1，相等0，递增为1，默认相等
            int result = 1;// 默认为一个序列
            int i = 1;
            for (i = 1; i < n; i++) {
                // 如果后一个大于前一个，即递增

                if (A[i] > A[i - 1]) {
                    // 如果原来是相等,标志为递增
                    if (flag == 0) flag = 1;
                        // 如果原来就是递减，增加一个子序列，标志恢复默认
                    else if (flag == -1) {
                        result++;
                        flag = 0;
                    }
                }
                    // 如果后一个小于前一个，即递减 }
                    else if (A[i] < A[i - 1]) {
                        // 如果原来是相等,标志为递减
                        if (flag == 0) flag = -1;
                            // 如果原来是递增,增加一个子序列，标志恢复默认
                        else if (flag == 1) {
                            result++;
                            flag = 0;
                        }
                    }
                    //其他情况就是往下继续遍历
                }


                      return result;
        }
                     public static void main(String[] args) {
                     // 输入包括一个整数n(1 ≤ n ≤ 1,000,000,000)
                      Scanner s = new Scanner(System.in);
                     // 输入的第一行为一个正整数n(1 ≤ n ≤ 10^5)
                      int n = s.nextInt();
                     // 第二行包括n个整数A_i(1 ≤ A_i ≤ 10^9),表示数组A的每个数字。
                      int[] A = new int[n];
                      for (int i = 0; i < n; i++){
                      A[i] = s.nextInt();
                     }
                     System.out.println(sonCount(A, n));
        }
    }


