import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-21
 * Time: 21:16
 */
public class text20 {

        public static void main(String[] args){
            Scanner sc=new Scanner(System.in);
            String str=sc.nextLine();
            char[] a=str.toCharArray();
            //先整个句子逆置
            a=nizhi(a,0,a.length-1);
            //每个单词逆置
            int start=0;
            int end=0;
            while(start<a.length && end<a.length){
                int i=start;
                while(i<a.length && a[i]!=' ') {
                    i++;
                    end = i - 1;
                    a = nizhi(a, start, end);
                    start = i + 1;
                    end = i + 1;
                }
            }
            for(int j=0;j<a.length;j++)
                System.out.print(a[j]);
        }
        //该函数的作用在于将a从start到end的部分进行逆置
        public static char[] nizhi (char[] a,int start, int end){
            while(start<end){
                char temp=a[start];
                a[start]=a[end];
                a[end]=temp;
                start++;
                end--;
            }
            return a;
        }

}
