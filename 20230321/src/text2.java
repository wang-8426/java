import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-21
 * Time: 20:55
 */
public class text2 {
    public static  char[] reserve(char[] a, int strat, int end) {
        while (strat < end) {
            char temp = a[strat];
            a[strat] = a[end];
            a[end] = temp;
            strat++;
            end--;
        }
        return a;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in) ;
        String str = sc.nextLine();
        char[] a = str.toCharArray();
        a = reserve(a,0,a.length-1);
        int start = 0;
        int end = 0;
        while (start<a.length && end<a.length) {
            int i = start;
            //遍历字符数组，提出单个单词，进行逆置
            while (i<a.length && a[i]!= ' ') {
                i++;
                end =  i-1;
            }
            a = reserve(a,start,end);
            start = i+1 ;
            end = i+1;
        }
        for(int j=0; j < a.length; j++) {
            System.out.print(a[j]);
        }

    }
}
