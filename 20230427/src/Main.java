import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-27
 * Time: 21:30
 */
public class Main {
    public static int getNums(int n) {
        if (n <= 1) return n;
        // 使用一个长度为 2 的数组存储前两位的值
        int[] nums = new int[] { 0, 1 };
        int result = 0;
        for (int i = 2; i <= n; i++) {
            result = nums[0] + nums[1];
            nums[0] = nums[1];
            nums[1] = result;
        }
        return result;
    }
    public static void main(String[] args) {

        Scanner sc =new Scanner(System.in);
        while (sc.hasNext()){
            int n = sc.nextInt();
            System.out.println(getNums(n));
        }
    }
}
