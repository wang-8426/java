import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-25
 * Time: 下午 09:25
 */
public class Main1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];


        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        Arrays.sort(arr);
        System.out.println(count(arr, n, 0, 0, 1));
    }

    public static int count(int[] arr, int n, int pos, int sum, int mult) {
        int count = 0;
        for (int i = pos; i < n; i++) {
            sum += arr[i];
            mult *= arr[i];
            if (sum > mult) {
                //符合幸运袋子 ，继续递归
                count = count + 1 + count(arr,n,i+1,sum,mult);
            } else if (arr[i] == 1) {
                //任何数与1相加 都大于与1相乘
                count = count  + count(arr,n,i+1,sum,mult);
            } else {
                break;
            }
            //
            //回溯
            sum -= arr[i];
            mult /= arr[i];
            while (i < n-1 && arr[i] == arr[i+1]) {
                i++;
            }
        }
        return count;
    }
}
