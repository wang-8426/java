import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-25
 * Time: 下午 09:25
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        int month = sc.nextInt();
        int day = sc.nextInt();
        int count = 0;
        int[] months = new int[]{31,28,31,30,31,30,31,31,30,31,30,31};
        if (isLeap(year)) {
            months[1] = 29;
        }
        for (int i = 0; i < month-1; i++) {
            count += months[i];
        }
        count += day;

        System.out.println(count);
    }

    private static boolean isLeap(int year) {
        if((year%4 == 0 && year%100!=0)||year%400 == 0) {
            return true;
        }
        return false;
    }
}
