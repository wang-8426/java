/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-25
 * Time: 23:23
 */
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:淘宝网店
 * User: WangWZ
 * Date: 2023-04-12
 * Time: 19:13
 */
public class Main {
    //判断是否是闰年
    private static boolean isLeapYear(int y) {
        return ((y % 400 == 0) || (y % 4 == 0 && y % 100 != 0));
    }
    //判断当前月数是否是素数
    private static boolean isPrime(int m) {
        if(m == 2 || m == 3 || m == 5 || m == 7 || m == 11) {
            return true;
        } else {
            return false;
        }
    }
    //整年全部的收益
    private static int yearSum(int y) {
        int sum =  31 * 2 + 28 * 1 + 31 * 1 + 30 * 2 +
                31 * 1 + 30 * 2 + 31 * 1 + 31 * 2 +
                30 * 2 + 31 * 2 + 30 * 1 + 31 * 2;
        if(isLeapYear(y)) {
            sum += 1;
        }
        return sum;
    }
    //指定日期之前的收益
    private static int beforeSum(int y,int m,int d) {
        int sum = 0;
        //1.先加当前月的没有过完整个月的天数的收益
        if(isPrime(m)) {
            sum += d * 1;
        } else {
            sum += d * 2;
        }
        //2.循环判断之前过完的月份是否是素数，并进行收益累加
        m--;
        while(m > 0) {
            switch(m){
                case 1 :case 8: case 10: case 12:
                    sum += 31 * 2;
                    break;
                case 2 :
                    sum += (isLeapYear(y)?29:28);
                    break;
                case 3 :case 5: case 7:
                    sum += 31 * 1;
                    break;
                case 4: case 6: case 9:
                    sum += 30 * 2;
                    break;
                case 11:
                    sum += 30 * 1;
                    break;
            }
            m--;
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int fromYear = scanner.nextInt();
            int fromMo = scanner.nextInt();
            int fromDa = scanner.nextInt();
            int toYear = scanner.nextInt();
            int toMo = scanner.nextInt();
            int toDa = scanner.nextInt();
            //from剩余的收益 = fromYear的全部收益 - from之前的收益
            int n1 = yearSum(fromYear) - beforeSum(fromYear,fromMo,fromDa - 1);
            //中间年份的收益：循环加中间年分的收益
            int n2 = 0;
            for(int i = fromYear+1; i < toYear; i++) {
                n2 += yearSum(i);
            }
            //to之前的收益
            int n3 = beforeSum(toYear,toMo,toDa);
            //总收益sum
            int sum = 0;
            if(fromYear == toYear) {
                sum = n1 + n3 - yearSum(fromYear);
            } else {
                sum = n1 + n2 + n3;
            }
            System.out.println(sum);

        }
    }
}