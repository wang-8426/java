import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-03-29
 * Time: 23:16
 */
public class Main {
    //无加号实现加法
    public static int add(int a, int b) {

            int sum = a ^ b;
            int carry = (a & b) << 1;

           return sum + carry;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int ret = add(a,b);
        System.out.println(ret);

    }
}
