import java.util.Arrays;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-11-16
 * Time: 21:54
 */
class Student {
    public String name;
    public int age;
    public int score;

    public Student(String name, int age, int score) {
        this.name = name;
        this.age = age;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", score=" + score +
                '}';
    }
}

class AgeComparator implements Comparator<Student>{

    public int compare(Student o1,Student o2) {
        return o1.age - o2.age;
    }
}
class NameComparator implements Comparator<Student> {
    public int compare(Student o1,Student o2) {
        return o1.name.compareTo(o2.name);
    }
}
public class test {
    public static void main(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("zhangsan",19,80);
        students[1] = new Student("lisi",15,75);
        students[2] = new Student("wangwu",17,85);

        AgeComparator ageComparator = new AgeComparator();
        Arrays.sort(students,ageComparator);
        NameComparator nameComparator = new NameComparator();
        Arrays.sort(students,nameComparator);
        System.out.println(Arrays.toString(students));
    }
}
