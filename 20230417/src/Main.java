import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-17
 * Time: 21:18
 */
public class Main {
    public static  int addDigits(int num) {
        if(num < 10) {
            return num;
        }
        int result = 0;
        while(num != 0) {
            result = result + num % 10;
            num = num / 10;
        }
        return addDigits(result);
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        while (sc.hasNext()) {
            n = sc.nextInt();
            System.out.println(addDigits(n));
        }


    }
}