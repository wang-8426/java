/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-09-03
 * Time: 下午 10:27
 */

//下降路径最小和
class Solution1 {
    public int minFallingPathSum(int[][] matrix) {
        int n = matrix.length;

        int[][] dp = new int[n+1][n+2];
        for(int i = 1;i <= n;i++) {
            dp[i][0] = Integer.MAX_VALUE;
            dp[i][n+1] = Integer.MAX_VALUE;
        }
        for(int i = 1;i <= n;i++){
            for(int j = 1;j <= n;j++){
                dp[i][j] = Math.min(dp[i-1][j],Math.min(dp[i-1][j-1],dp[i-1][j+1]))                          + matrix[i-1][j-1];
            }
        }
        int ret = Integer.MAX_VALUE ;
        for (int j = 1;j <= n;j++) {
            ret = Math.min(ret,dp[n][j]);
        }
        return ret;

    }
}