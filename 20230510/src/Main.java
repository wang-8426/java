import java.util.Scanner;

public class Main {

    public static int solve(String text1, String text2) {
        int len1 = text1.length();
        int len2 = text2.length();

        // DP数组
        int[][] dp = new int[len1 + 1][len2 + 1];

        // 计算所有DP值
        for (int i = 1; i <= len1; i++) {
            char c1 = text1.charAt(i - 1);
            for (int j = 1; j <= len2; j++) {
                char c2 = text2.charAt(j - 1);
                if (c1 == c2) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }

        return dp[len1][len2];
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()){
            String s = sc.nextLine();
            String []g = s.split(" ");
            String s1 = g[0];
            String s2 = g[1];
            System.out.println(solve(s1,s2));
        }
    }

}
