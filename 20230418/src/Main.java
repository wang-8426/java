import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-04-18
 * Time: 22:48
 */
public class Main {
    public static boolean judge(BigDecimal n, BigDecimal r){
        BigDecimal c = new BigDecimal("6.28").multiply(r);

        return n.compareTo(c) == 1 ? false : true;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BigDecimal n ;
        BigDecimal r ;
        while (sc.hasNext()) {
            n = sc.nextBigDecimal();
            r = sc.nextBigDecimal();
            if (judge (n , r)) {
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
        }
    }
}
