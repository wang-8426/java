/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-09-04
 * Time: 下午 10:48
 */
class Solution {
    public int calculateMinimumHP(int[][] d) {
        int m = d.length;
        int n = d[0].length;
        int[][] dp = new int[m+1][n+1];

        //初始化
        for(int j = 0;j <= n;j++) {
            dp[m][j] = Integer.MAX_VALUE;
        }
        for(int i = 0;i <= m;i++) {
            dp[i][n] = Integer.MAX_VALUE;
        }
        dp[m][n-1] = dp[m-1][n] = 1;

        //填表
        for(int i = m-1;i >= 0;i--){
            for(int j = n-1;j >= 0;j--){
                dp[i][j] = Math.min(dp[i][j+1],dp[i+1][j]) - d[i][j];
                dp[i][j] = Math.max(1,dp[i][j]);
            }
        }
        return dp[0][0];

    }
}
