import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-14
 * Time: 下午 06:21
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuilder A = new StringBuilder(sc.nextLine());
        StringBuilder B = new StringBuilder(sc.nextLine());
        int count = 0;
        for (int i = 0; i < A.length()+1; i++) {
            StringBuilder temp = new StringBuilder(A);
            temp.insert(i,B);
            if(isReverse(temp)) {
                count++;
            }
        }
        System.out.println(count);
    }
    //判断是否为回文
    public static boolean isReverse (StringBuilder s) {

        String s1 = s.toString();
        StringBuilder s2 = s.reverse();
        return s1.equals(s2.toString());
    }
}
