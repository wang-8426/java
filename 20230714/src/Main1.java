

import java.util.*;
public class Main1{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = Integer.parseInt(sc.nextLine());
            int[] arr = new int[n];
            for(int i =0; i < n; i++){
                arr[i] = sc.nextInt();
            }
            int maxSum = arr[0];
            int sum = 0;
            for(int i = 0; i < n; i++){
                sum = sum + arr[i];
                sum = Math.max(arr[i],sum);
                maxSum = Math.max(maxSum,sum);
            }
            System.out.println(maxSum);
        }
        sc.close();
    }
}