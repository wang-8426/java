import java.util.Scanner;
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2022-10-30
 * Time: 15:21
 */
public class test {
    //打印 100以内能被3和5整除的数
    public static void main1(String[] args) {
        int a = 100;
        for (int i = 1; i < a; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println(i);
                continue;
            }
        }
    }

    //打印100以内素数
    public static void main2(String[] args) {
        for (int i = 1; i < 100; i++) {
            int count = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    count++;
                }
            }
            if (count == 2) {
                System.out.println(i);
            }
        }
    }

    //    1到 100 的所有整数中出现多少个数字9
    public static void main3(String[] args) {
        int count = 0;
        for (int i = 0; i < 100; i++) {
            if (i % 10 == 9 || i / 10 == 9) {
                count++;
            }
        }
        System.out.println(count);
    }

    //    输出 1000 - 2000 之间所有的闰年
    public static void main4(String[] args) {
        int count = 0;
        for (int i = 1000; i < 2000; i++) {
            if ((i % 4 == 0 && i % 100 != 0) || (i % 400 == 0)) {
                System.out.println( i );
            }
        }
    }


    //判定素数
    public static void main5(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("输入num");
        int num = scan.nextInt();

        int count = 0;
        for (int j = 1; j <= num; j++) {
            if (num % j == 0) {
                count++;
            }
        }
        if (count == 2) {
            System.out.println(num + "是素数");
        } else {
            System.out.println(num + "不是素数");
        }
    }

}

