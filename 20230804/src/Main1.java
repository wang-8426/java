/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-04
 * Time: 下午 10:57
 */
import java.util.*;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int total = 0;
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()) { // 注意 while 处理多个 case
            String str1 = in.next();
            String str2 = in.next();
            int[] aa = new int[128];
            int[] bb = new int[128];
            for (int i = 0; i < str1.length(); i++) {
                int num1 = (int)str1.charAt(i);
                aa[num1] += 1;
            }
            for (int i = 0; i < str2.length(); i++) {
                int num2 = (int)str2.charAt(i);
                bb[num2] += 1;
            }

            for (int i = 0; i < 128; i++) {
                if (!(aa[i] == 0 && bb[i] == 0)) {
                    total += Math.abs(aa[i] - bb[i]);
                }
            }
            System.out.println(total);
        }
    }
}


