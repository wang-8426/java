/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-04
 * Time: 下午 10:38
 */
public class Main {
    public static void main(String[] args) {

    }
    public static int getValue(int[] gifts, int n) {
        // write code here
        int flag = 1;
        int num = gifts[0];
        for (int i = 1; i < n; i++) {
            if (gifts[i] == num) {
                flag++;
            } else {
                flag--;
            }
            if (flag == 0) {
                num = gifts[i];
            }
        }
        flag = 0;
        for (int i = 0; i < n; i++) {
            if(gifts[i] == num) {
                flag++;
            }
        }
        if(flag > n/2) {
            return num;
        } else {
            return 0;
        }
    }

}
