import java.util.*;
public class Main{
    public static boolean isRn(int year) {//判断是不是闰年
        if(((year%4==0)&&year%100!=0)||(year%400==0)){
            return true;
        }
        return false;

    }
    public static   int day_of_week(int year, int month, int day)//蔡勒公式，给年月日计算出这天是星期几
    {
        if (month == 1 || month == 2)
        {
            month += 12;
            year -= 1;
        }

        int century = year / 100;
        year %= 100;
        int week = year + (year / 4) + (century / 4) - 2 * century + 26 * (month + 1) / 10 + day -
                1;
        week = (week % 7 + 7) % 7;

        if (week == 0)
        {
            week = 7;
        }

        return week;
    }

    public static void printFunc(int y,int m,int d) {
        String str1="-";
        String str2="-";
        boolean f1=false;
        boolean f2=false;
        if(m/10==0){
            f1=true;
            str1+=0;
        }
        if(d/10==0){
            f2=true;
            str2+=0;
        }
        System.out.println(y + str1 + m + str2 + d);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int y = scanner.nextInt();
            boolean f=isRn(y);
            for (int m = 1; m <= 12; ) {
                int x = 0;
                if (m % 2 == 0) {//偶数月
                    if (m == 2) {
                        if (f) {
                            x = 29;
                        } else {
                            x = 28;
                        }
                    } else {
                        x = 30;
                    }
                } else {
                    x = 31;
                }
                int tmp=day_of_week(y,m,1);//这年这月第一天是星期几
                boolean flg=false;//后面用于标记劳动节的
                for (int d = 1; d <= x; d++) {
                    int j=(d+tmp)/7+1;//第几个星期
                    // 注意，第几个星期！=第几个星期几
                    //比如你这个月是星期4是1号，那你第一个星期3，是第二个星期才出现的

                    int w = day_of_week(y,m,d);//这年这月这日是星期几
                    //蔡勒公式，给一个年月日计算是星期几

                    if ((m == 1 && d == 1) || (m == 7 && d == 4) || (m == 12 && d == 25)) {//指定节日
                        if(m<10){
                            System.out.println(y + "-0" + m + "-0" + d);
                        }else{
                            System.out.println(y + "-" + m + "-" + d);
                        }
                        continue;
                    } else if (m == 1  && w == 1) {//马丁路德金纪念日
                        if(1<tmp){
                            //比如你这个月是星期3是1号，那你第三个星期1,是第四个星期才出现的
                            if(4==j){
                                printFunc(y,m,d);
                            }
                        }else{//这个月第一天就是星期1，那第三个星期1，就是第三个星期出现
                            if(3==j){
                                printFunc(y,m,d);
                            }
                        }
                    } else if (m == 2  && w == 1) {//总统节
                        if(1<tmp){
                            if(4==j){
                                printFunc(y,m,d);
                            }
                        }else{
                            if(3==j){
                                printFunc(y,m,d);
                            }
                        }
                    } else if (m == 5  && w == 1) {//阵亡将士纪念日
                        //五月第一天是星期一，最后一个星期一是在 第5个星期
                        //五月第一天是星期日，最后一个星期一是在 第6个星期
                        int k=0;

                        for(int a=0;a<7;a++){
                            if (flg==true){
                                break;
                            }
                            if(day_of_week(y,5,31-a)==1){
                                printFunc(y,m,31-a);
                                flg=true;
                            }
                        }
                    } else if (m == 9 && w == 1) {//劳动节
                        if(1<tmp){
                            if(2==j){
                                printFunc(y,m,d);
                            }
                        }else{
                            if(1==j){
                                printFunc(y,m,d);
                            }
                        }
                    } else if (m == 11 && w == 4) {//感恩节
                        if(4<tmp){
                            if(5==j){
                                printFunc(y,m,d);
                            }
                        }else{
                            if(4==j){
                                printFunc(y,m,d);
                            }
                        }
                    }
                }
                m++;
            }

            System.out.println();
        }
    }
}
