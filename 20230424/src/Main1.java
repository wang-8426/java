

import java.util.Scanner;

public class Main1 {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextInt()){
            int x = sc.nextInt();
            System.out.print(x+"=");
            int i=2;
            while(i<=x){		//使用循环来找到可以被整除的数，然后通过out函数输出
                if(i == x){		//如果相等的话，就说明这个数没有因数，只有1和它自己；
                    System.out.println(i);
                    break;
                }else if(x % i ==0){		//如果这个数有因数，然后找到除去这个因数后的值，继续循环
                    System.out.print(i+"*");
                    x=x/i;
                }else{			//如果都不满足，则继续循环
                    i++;
                }
            }
        }
    }

}

