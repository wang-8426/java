import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-11
 * Time: 21:25
 */
public class demo1 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int [] arr = new int[n+1];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        int j = 0;
        int count = 0;
        while (j < n) {
            if(arr[j] < arr[j+1]) {
                while (j<n && arr[j]<arr[j+1]) {
                    j++;
                }
                count++;
                j++;
            } else if (arr[j] == arr[j+1]) {
                j++;
            } else  {
                while (j<n && arr[j]>arr[j+1]) {
                    j++;
                }
                count++;
                j++;
            }
        }
        System.out.println(count);
    }
}
