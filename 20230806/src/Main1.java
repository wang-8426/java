import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-06
 * Time: 下午 06:46
 */
public class Main1 {
    //定义全局变量，用来收集结果
    static StringBuffer sb = new StringBuffer();
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()) {
            int[][] arr = new int[in.nextInt()][in.nextInt()];
            for (int i = 0; i < arr.length; i++) {
                for (int j = 0; j < arr[0].length; j++) {
                    arr[i][j] = in.nextInt();
                }
            }
            dfs(arr,0,0,"");
            System.out.println(sb.toString());
        }
    }
    public static boolean dfs(int[][] arr,int i ,int j,String result){
        //下标越界
        if(i>arr.length-1||j>arr[0].length-1||i<0||j<0){
            return false;
        }
        //走出迷宫，使用全局变量收集结果。
        if(i==arr.length-1 && j==arr[0].length-1){
            sb.append(result);
            sb.append("("+i+","+j+")");
            return true;
        }
        //如果为0，表示可以走
        if(arr[i][j]==0){
            //标记已走过的路
            arr[i][j]=2;
            //result回溯结果
            if(dfs(arr,i-1,j,result+"("+i+","+j+")\n")){
                return true;
            }
            if(dfs(arr,i+1,j,result+"("+i+","+j+")\n")){
                return true;
            }
            if(dfs(arr,i,j-1,result+"("+i+","+j+")\n")){
                return true;
            }
            if(dfs(arr,i,j+1,result+"("+i+","+j+")\n")){
                return true;
            }
            //回溯
            arr[i][j]=0;
        }
        return false;
    }
}