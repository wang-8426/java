/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-08-06
 * Time: 下午 06:45
 */
public class Main {
    public int getMost(int[][] board) {
        int[][] dp = new int[7][7];
        dp[0][0] = 0;
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j <= 6; j++) {
                dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]) + board[i - 1][j - 1];
            }
        }
        return dp[6][6];
    }
}
