/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 念秋
 * Date: 2023-07-20
 * Time: 上午 09:40
 */
import java.io.*;
import java.util.*;

public class Main1{
    public static void main(String[] args) throws Exception{
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = sc.nextInt();
            int m = sc.nextInt();
            int[][] dp = new int[n + 1][m + 1];
            for(int i = 0; i <= n; ++i){
                for(int j = 0; j <= m ; ++j){
                    if(i == 0 || j == 0){
                        dp[i][j] = 1;
                    }else{
                        dp[i][j] = dp[i][j - 1] + dp[i - 1][j];
                    }
                }
            }
            System.out.println(dp[n][m]);
        }
    }
}
